CREATE TABLE Users(
	user_id varchar(16) not null,
	role varchar(16),
	PRIMARY KEY(user_id)
);

CREATE TABLE Members(
	members_user_id varchar(16) not null,
	borrow_num varchar(16),
	PRIMARY KEY(members_user_id)
);

CREATE TABLE Librarians(
	librarians_user_id varchar(16) not null,
	username varchar(16),
	password varchar(16),
	PRIMARY KEY(librarians_user_id)
);

CREATE TABLE Documents(
	doc_id varchar(16) not null,
	title varchar(128),
	user_id varchar(16),
	PRIMARY KEY(doc_id),
);

CREATE TABLE Books(
	books_doc_id varchar(16) not null,
	edition varchar(64),
	year DATE,
	PRIMARY KEY(books_doc_id)
)

CREATE TABLE JournalArticles(
	journal_articles_doc_id varchar(16) not null,
	journal_id varchar(16),
	PRIMARY KEY(journal_articles_doc_id),
	FOREIGN KEY (journal_articles_doc_id) REFERENCES documents
	                                      ON UPDATE CASCADE
										  ON DELETE NO ACTION,
	FOREIGN KEY (journal_id) REFERENCES Journal
                             ON UPDATE CASCADE
							 ON DELETE NO ACTION
);

CREATE TABLE Magazines(
	magazines_doc_id varchar(16) not null,
	name varchar(32),
	publish_time DATE,
	issue_id varchar(16),
	PRIMARY KEY(magazines_doc_id),
	FOREIGN KEY (magazines_doc_id) REFERENCES Documents
	                               ON UPDATE CASCADE
								   ON DELETE NO ACTION,
	FOREIGN KEY (issue_id) REFERENCES MagazineIssue
	                       ON UPDATE NO ACTION
						   ON DELETE NO ACTION
);

CREATE TABLE Journal(
	journal_id varchar(16) not null,
	title varchar(32),
	type varchar(16),
	issue_id varchar(16),
	PRIMARY KEY(journal_id),
	FOREIGN KEY (issue_id) REFERENCES JournalIssue
	                       ON UPDATE NO ACTION
						   ON DELETE NO ACTION
);

CREATE TABLE Author(
	author_name varchar(32) not null,
	address varchar(64),
	PRIMARY KEY (author_name)
);

CREATE TABLE Publisher(
	publisher_name varchar(32) not null,
	address varchar(64),
	phone varchar(16),
	PRIMARY KEY (publisher_name)
);

CREATE TABLE Issue(
	issue_id varchar(16) not null 
	PRIMARY KEY (issue_id)
);

CREATE TABLE JournalIssue(
	issue_id varchar(16) not null,
	date DATE,
	PRIMARY KEY (issue_id)
);

CREATE TABLE MagazineIssue(
	issue_id varchar(16) not null,
	title varchar(64),
	PRIMARY KEY (issue_id)
);

CREATE TABLE copies(
	copy_id varchar(16) not null,
	location varchar(32) not null,
	borrowed varchar(32) not null,
	doc_id varchar(16) not null,
	members_user_id varchar(16) not null,
	PRIMARY KEY(copy_id, location, borrowed, doc_id),
	FOREIGN KEY (doc_id) REFERENCES Documents
	                     ON UPDATE CASCADE
						 ON DELETE CASCADE
);

CREATE TABLE manage(
	librarians_user_id varchar(16) not null,
	members_user_id varchar(16) not null,
	PRIMARY KEY(librarians_user_id, members_user_id),
	FOREIGN KEY (librarians_user_id) REFERENCES Librarians,
	FOREIGN KEY (members_user_id) REFERENCES Members
);

CREATE TABLE lib_doc(
	doc_id varcha(16) not null,
	librarians_user_id varchar(16) not null,
	PRIMARY KEY(doc_id, librarians_user_id),
	FOREIGN KEY (doc_id) REFERENCES Documents
	                     ON UPDATE NO ACTION
						 ON DELETE NO ACTION,
	FOREIGN KEY (librarians_user_id) REFERENCES Librarians
	                                 ON UPDATE NO ACTION
									 ON DELETE NO ACTION
);

CREATE TABLE cite(
	doc_id varchar(16) not nul,
	cite_id varchar(16) not null,
	PRIMARY KEY(doc_id, cite_id),
	FOREIGN KEY (doc_id) REFERENCES Documents
	                     ON UPDATE NO ACTION
						 ON DELETE NO ACTION
);

CREATE TABLE written_by(
	books_doc_id varchar(16) not null,
	journal_articles_doc_id varchar(16) not null,
	author_name varchar(16) not null,
	PRIMARY KEY(books_doc_id, journal_articles_doc_id, author_name),
	FOREIGN KEY (author_name) REFERENCES author
	                          ON UPDATE NO ACTION
							  ON DELETE NO ACTION

);

CREATE TABLE published_by(
	publisher_name varchar(32) not null,
	journal_id varchar(16) not null,
	books_doc_id varchar(16) not null,
	PRIMARY KEY(publisher_name, journal_id, books_doc_id),
	FOREIGN KEY (publisher_name) REFERENCES Publisher
	                             ON UPDATE NO ACTION
								 ON DELETE NO ACTION,
	FOREIGN KEY (journal_id) REFERENCES Journal
	                         ON UPDATE NO ACTION
							 ON DELETE NO ACTION,
	FOREIGN KEY (books_doc_id) REFERENCES Books
	                           ON UPDATE NO ACTION
							   ON DELETE NO ACTION
);

CREATE TABLE documentsdescription(
	types varchar(32) not null,
	keywords varchar(64) not null,
	classification varchar(32) not null,
	doc_id varchar(16) not null,
	PRIMARY KEY(types, keywords, classification, doc_id),
	FOREIGN KEY (doc_id) REFERENCES Documents
	                     ON UPDATE NO ACTION
						 ON DELETE NO ACTION
);

CREATE TABLE contributor(
	contributor varchar(32) not null,
	issue_id varchar(16) not null,
	PRIMARY KEY(contributor, issue_id),
	FOREIGN KEY (issue_id) REFERENCES MagazineIssue
	                       ON UPDATE NO ACTION
						   ON DELETE NO ACTION
);

CREATE TABLE editor(
	editor varchar(32) not null,
	issue_id varchar(16) not null,
	PRIMARY KEY(editor, issue_id),
	FOREIGN KEY (issue_id) REFERENCES Issue
	                       ON UPDATE NO ACTION
						   ON DELETE NO ACTION
);

CREATE VIEW book_view AS
   SELECT b.books_doc_id,
    b.edition,
    b.year,
    w.author_name,
    d.title,
    dd.types,
    dd.keywords,
    dd.classification,
    ( SELECT count(1) AS count
           FROM copies cp
          WHERE b.books_doc_id::text = cp.doc_id::text AND cp.borrowed::text = '0'::text) AS borrowed
   FROM books b
     LEFT JOIN writtenby w ON b.books_doc_id::text = w.books_doc_id::text
     LEFT JOIN documents d ON d.doc_id::text = b.books_doc_id::text
     LEFT JOIN documentdescription dd ON b.books_doc_id::text = dd.doc_id::text;


CREATE VIEW journalarticles_view AS
 SELECT ja.journal_articles_doc_id,
    d.title,
    j.journal_id,
    j.title AS journaltitle,
    j.type AS journaltype,
    j.issue_id,
    w.author_name,
    dd.types,
    dd.keywords,
    dd.classification,
    ( SELECT count(1) AS count
           FROM copies cp
          WHERE ja.journal_articles_doc_id::text = cp.doc_id::text AND cp.borrowed::text = '0'::text) AS borrowed
   FROM journalarticles ja
     LEFT JOIN journal j ON ja.journal_id::text = j.journal_id::text
     LEFT JOIN writtenby w ON ja.journal_articles_doc_id::text = w.journal_articles_doc_id::text
     LEFT JOIN documents d ON ja.journal_articles_doc_id::text = d.doc_id::text
     LEFT JOIN documentdescription dd ON ja.journal_articles_doc_id::text = dd.doc_id::text;


CREATE VIEW magazines_view AS
 SELECT m.magazines_doc_id,
    m.name AS magzinename,
    d.title,
    m.publish_time,
    e.editor,
    mi.title AS issutitle,
    dd.types,
    dd.keywords,
    dd.classification,
    ( SELECT count(1) AS count
           FROM copies cp
          WHERE m.magazines_doc_id::text = cp.doc_id::text AND cp.borrowed::text = '0'::text) AS borrowed
   FROM magazines m
     LEFT JOIN editor e ON e.issue_id::text = m.issue_id::text
     LEFT JOIN magazineissue mi ON mi.issue_id::text = m.issue_id::text
     LEFT JOIN documents d ON m.magazines_doc_id::text = d.doc_id::text
     LEFT JOIN documentdescription dd ON m.magazines_doc_id::text = dd.doc_id::text;
