# CS425 21FALL ch_Group6 Project
## Group Members
- Sidi Li (A20479715)
- Yan Zhuang (A20479179)
- Tianye Yang (A20482352)

## Quick Start
1. Open pgAdmin
2. Create a role named "apple" with password "123456"
3. Create a new database named "apple"
4. Restore the database using "CS425DB_BACKUP" in the "Release" folder
5. Start the server by running the following command:`` java -jar 425test.jar``
6. Unzip the "build.zip", and open "index.html".

## Project Environment
- Java version "13.0.2" 2020-01-14
- Java(TM) SE Runtime Environment (build 13.0.2+8)
- Java HotSpot(TM) 64-Bit Server VM (build 13.0.2+8, mixed mode, sharing)
- npm 6.14.13

