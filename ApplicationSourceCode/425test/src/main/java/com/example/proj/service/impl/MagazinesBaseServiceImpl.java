package com.example.proj.service.impl;

import com.example.common.service.impl.BaseService;
import com.example.proj.pojo.MagazinesBase;
import com.example.proj.pojo.WrittenbyKey;
import com.example.proj.service.MagazinesBaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("MagazinesBaseServiceImpl")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class MagazinesBaseServiceImpl extends BaseService<MagazinesBase> implements MagazinesBaseService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<MagazinesBase> findAll() {
        try {
            Example example = new Example(MagazinesBase.class);
            example.setOrderByClause("magazines_doc_id asc");
            return this.selectByExample(example);
        } catch (Exception e) {
            log.error("query failed", e );
            return new ArrayList<>();
        }
    }

    @Override
    public void update(MagazinesBase magazinesBase) {
        this.updateNotNull(magazinesBase);
    }

    @Override
    public void delete(String Ids) {
        List<String> list = Arrays.asList(Ids.split(","));
        this.batchDelete(list, "magazinesDocId", MagazinesBase.class);
    }

    @Override
    public void add(MagazinesBase magazinesBase) {
        this.save(magazinesBase);
    }
}
