package com.example.proj.service;

import com.example.common.service.IService;
import com.example.proj.pojo.Books;

import java.util.List;

public interface BooksService extends IService<Books> {

    List<Books> findAll();

    List<Books> findByConditions(String author, String title, String classification, String copyborrowed);

    List<Books> havingKeywords(String keywords);
}
