package com.example.proj.pojo;

import java.util.Date;
import javax.persistence.*;

public class Copies extends CopiesKey {
    private String location;

    private String borrowed;

    @Column(name = "members_user_id")
    private String membersUserId;

    @Column(name = "due_date")
    private Date dueDate;

    /**
     * @return location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location
     */
    public void setLocation(String location) {
        this.location = location == null ? null : location.trim();
    }

    /**
     * @return borrowed
     */
    public String getBorrowed() {
        return borrowed;
    }

    /**
     * @param borrowed
     */
    public void setBorrowed(String borrowed) {
        this.borrowed = borrowed == null ? null : borrowed.trim();
    }

    /**
     * @return members_user_id
     */
    public String getMembersUserId() {
        return membersUserId;
    }

    /**
     * @param membersUserId
     */
    public void setMembersUserId(String membersUserId) {
        this.membersUserId = membersUserId == null ? null : membersUserId.trim();
    }

    /**
     * @return due_date
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * @param dueDate
     */
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
}