package com.example.proj.dao;

import com.example.common.config.MyMapper;
import com.example.proj.pojo.Magazines;

public interface MagazinesMapper extends MyMapper<Magazines> {
}