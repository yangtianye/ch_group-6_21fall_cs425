package com.example.proj.pojo;

import java.util.Date;
import javax.persistence.*;

@Table(name = "book_view")
public class Books {
    @Column(name = "books_doc_id")
    private String booksDocId;

    private String edition;

    private Date year;

    @Column(name = "author_name")
    private String authorName;

    private String title;

    private String types;

    private String keywords;

    private String classification;

    private String borrowed;

    /**
     * @return books_doc_id
     */
    public String getBooksDocId() {
        return booksDocId;
    }

    /**
     * @param booksDocId
     */
    public void setBooksDocId(String booksDocId) {
        this.booksDocId = booksDocId == null ? null : booksDocId.trim();
    }

    /**
     * @return edition
     */
    public String getEdition() {
        return edition;
    }

    /**
     * @param edition
     */
    public void setEdition(String edition) {
        this.edition = edition == null ? null : edition.trim();
    }

    /**
     * @return year
     */
    public Date getYear() {
        return year;
    }

    /**
     * @param year
     */
    public void setYear(Date year) {
        this.year = year;
    }

    /**
     * @return author_name
     */
    public String getAuthorName() {
        return authorName;
    }

    /**
     * @param authorName
     */
    public void setAuthorName(String authorName) {
        this.authorName = authorName == null ? null : authorName.trim();
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * @return types
     */
    public String getTypes() {
        return types;
    }

    /**
     * @param types
     */
    public void setTypes(String types) {
        this.types = types == null ? null : types.trim();
    }

    /**
     * @return keywords
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * @param keywords
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    /**
     * @return classification
     */
    public String getClassification() {
        return classification;
    }

    /**
     * @param classification
     */
    public void setClassification(String classification) {
        this.classification = classification == null ? null : classification.trim();
    }

    /**
     * @return borrowed
     */
    public String getBorrowed() {
        return borrowed;
    }

    /**
     * @param borrowed
     */
    public void setBorrowed(String borrowed) {
        this.borrowed = borrowed == null ? null : borrowed.trim();
    }
}