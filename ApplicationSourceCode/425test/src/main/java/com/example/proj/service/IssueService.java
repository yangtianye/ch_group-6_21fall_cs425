package com.example.proj.service;

import com.example.common.service.IService;
import com.example.proj.pojo.Issue;

import java.util.List;

public interface IssueService extends IService<Issue> {
    List<Issue> findAll ();

    void update(Issue issue);

    void delete(String Ids);

    void add(Issue issue);
}
