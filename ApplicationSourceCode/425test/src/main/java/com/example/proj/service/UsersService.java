package com.example.proj.service;

import com.example.common.service.IService;
import com.example.proj.pojo.Users;

import java.util.List;

public interface UsersService extends IService<Users> {
    List<Users> findAll (Users users);

    Users findById(Long id);

    void update(Users users);

    void delete(String Ids);

    void add(Users users);
}
