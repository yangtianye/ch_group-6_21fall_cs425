package com.example.proj.service;

import com.example.common.service.IService;
import com.example.proj.pojo.WrittenbyKey;

import java.util.List;

public interface WrittenByService extends IService<WrittenbyKey> {
    List<WrittenbyKey> findAll ();

    void update(WrittenbyKey writtenbyKey);

    void delete(String Ids);

    void add(WrittenbyKey writtenbyKey);
}
