package com.example.proj.controller;

import com.example.common.controller.BaseController;
import com.example.common.domain.ResponseResult;
import com.example.proj.pojo.MagazineIssue;
import com.example.proj.service.MagazineIssueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@CrossOrigin(origins = "*",maxAge = 3600)
@Controller
public class MagazineIssueController extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MagazineIssueService magazineIssueService;

    @RequestMapping("/mi/list")
    @ResponseBody
    public List getList() {
        return this.magazineIssueService.findAll();
    }

    @RequestMapping("mi/delete")
    @ResponseBody
    public ResponseResult deleteDocuments(String ids) {
        try {
            this.magazineIssueService.delete(ids);
            return ResponseResult.ok(" delete successfully!");
        } catch (Exception e) {
            log.error(" delete failed!", e);
            return  ResponseResult.error(" delete failed!");
        }
    }

    @RequestMapping("mi/add")
    @ResponseBody
    public ResponseResult addDocuments(MagazineIssue magazineIssue) {
        try {
            this.magazineIssueService.add(magazineIssue);
            return ResponseResult.ok(" add successfully!");
        } catch (Exception e) {
            log.error(" add failed!", e);
            return ResponseResult.error(" add failed!");
        }
    }

    @RequestMapping("mi/update")
    @ResponseBody
    public ResponseResult updateDocuments(MagazineIssue magazineIssue) {
        try {
            this.magazineIssueService.update(magazineIssue);
            return ResponseResult.ok(" update successfully!");
        } catch (Exception e) {
            log.error(" update failed!", e);
            return ResponseResult.error(" update failed!");
        }

    }

}
