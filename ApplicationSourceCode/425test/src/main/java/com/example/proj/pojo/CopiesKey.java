package com.example.proj.pojo;

import javax.persistence.*;

@Table(name = "copies")
public class CopiesKey {
    @Id
    @Column(name = "copy_id")
    private String copyId;

    @Id
    @Column(name = "doc_id")
    private String docId;

    /**
     * @return copy_id
     */
    public String getCopyId() {
        return copyId;
    }

    /**
     * @param copyId
     */
    public void setCopyId(String copyId) {
        this.copyId = copyId == null ? null : copyId.trim();
    }

    /**
     * @return doc_id
     */
    public String getDocId() {
        return docId;
    }

    /**
     * @param docId
     */
    public void setDocId(String docId) {
        this.docId = docId == null ? null : docId.trim();
    }
}