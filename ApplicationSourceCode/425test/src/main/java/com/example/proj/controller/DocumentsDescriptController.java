package com.example.proj.controller;

import com.example.common.controller.BaseController;
import com.example.common.domain.ResponseResult;
import com.example.proj.pojo.DocumentdescriptionKey;
import com.example.proj.pojo.Documents;
import com.example.proj.service.DocumentsDescriptService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@CrossOrigin(origins = "*",maxAge = 3600)
@Controller
public class DocumentsDescriptController extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DocumentsDescriptService documentsDescriptService;

    @RequestMapping("/documentsDes/list")
    @ResponseBody
    public List getList() {
        return this.documentsDescriptService.findAll();
    }

    @RequestMapping("documentsDes/delete")
    @ResponseBody
    public ResponseResult deleteDocuments(String ids) {
        try {
            this.documentsDescriptService.delete(ids);
            return ResponseResult.ok("DocumentsDescription delete successfully!");
        } catch (Exception e) {
            log.error("DocumentsDescription delete failed!", e);
            return  ResponseResult.error("DocumentsDescription delete failed!");
        }
    }

    @RequestMapping("documentsDes/add")
    @ResponseBody
    public ResponseResult addDocuments(DocumentdescriptionKey documents) {
        try {
            this.documentsDescriptService.add(documents);
            return ResponseResult.ok("DocumentsDescription add successfully!");
        } catch (Exception e) {
            log.error("DocumentsDescription add failed!", e);
            return ResponseResult.error("DocumentsDescription add failed!");
        }
    }

    @RequestMapping("documentsDes/update")
    @ResponseBody
    public ResponseResult updateDocuments(DocumentdescriptionKey documents) {
        try {
            this.documentsDescriptService.update(documents);
            return ResponseResult.ok("DocumentsDescription update successfully!");
        } catch (Exception e) {
            log.error("DocumentsDescription update failed!", e);
            return ResponseResult.error("DocumentsDescription update failed!");
        }

    }
}
