package com.example.proj.service;

import com.example.common.service.IService;
import com.example.proj.pojo.JournalArticlesBase;

import java.util.List;

public interface JournalArticleBaseService extends IService<JournalArticlesBase> {
    List<JournalArticlesBase> findAll ();

    void update(JournalArticlesBase journalArticleBase);

    void delete(String Ids);

    void add(JournalArticlesBase journalArticleBase);
}
