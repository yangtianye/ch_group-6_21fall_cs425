package com.example.proj.dao;

import com.example.common.config.MyMapper;
import com.example.proj.pojo.MagazinesBase;

public interface MagazinesBaseMapper extends MyMapper<MagazinesBase> {
}