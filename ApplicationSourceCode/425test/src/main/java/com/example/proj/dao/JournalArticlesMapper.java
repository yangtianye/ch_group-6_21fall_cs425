package com.example.proj.dao;

import com.example.common.config.MyMapper;
import com.example.proj.pojo.JournalArticles;

public interface JournalArticlesMapper extends MyMapper<JournalArticles> {
}