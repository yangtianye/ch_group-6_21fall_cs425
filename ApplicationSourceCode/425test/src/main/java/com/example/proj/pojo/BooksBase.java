package com.example.proj.pojo;

import java.util.Date;
import javax.persistence.*;

@Table(name = "books")
public class BooksBase {
    @Id
    @Column(name = "books_doc_id")
    private String booksDocId;

    private String edition;

    private Date year;

    /**
     * @return books_doc_id
     */
    public String getBooksDocId() {
        return booksDocId;
    }

    /**
     * @param booksDocId
     */
    public void setBooksDocId(String booksDocId) {
        this.booksDocId = booksDocId == null ? null : booksDocId.trim();
    }

    /**
     * @return edition
     */
    public String getEdition() {
        return edition;
    }

    /**
     * @param edition
     */
    public void setEdition(String edition) {
        this.edition = edition == null ? null : edition.trim();
    }

    /**
     * @return year
     */
    public Date getYear() {
        return year;
    }

    /**
     * @param year
     */
    public void setYear(Date year) {
        this.year = year;
    }
}