package com.example.proj.service.impl;

import com.example.common.service.impl.BaseService;
import com.example.proj.dao.CopiesMapper;
import com.example.proj.pojo.Copies;
import com.example.proj.service.CopiesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("CopiesServiceImpl")
public class CopiesServiceImpl extends BaseService<Copies> implements CopiesService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    private static final int SUCCESS = 0;
    private static final int FAILED = 1;
    private static final int EXCEPTION = 2;

    @Autowired
    private CopiesMapper copiesMapper;

    @Override
    public List<Copies> havingCopies(String docId) {
        try{
            Example example = new Example(Copies.class);
            Example.Criteria criteria = example.createCriteria();
            if (!(("").equals(docId) || null == docId)) {
                criteria.andCondition("borrowed = '0' and doc_id = '" + docId + "'");
                example.setOrderByClause("copy_id");
            }
            else
                return new ArrayList<>();
            List<Copies> copies = this.selectByExample(example);
            return copies;
        }catch (Exception e) {
            log.error("query failed!", e );
            return new ArrayList<>();
        }
    }

    @Override
    public List<Copies> userDue(String userId) {
        try{
            Example example = new Example(Copies.class);
            Example.Criteria criteria = example.createCriteria();
            if (!(("").equals(userId) || null == userId)) {
                criteria.andCondition(" members_user_id = '" + userId + "' and due_date < current_date ");
            }
            else
                return new ArrayList<>();
            List<Copies> copies = this.selectByExample(example);
            return copies;

        }catch (Exception e) {
            log.error("query failed!", e );
            return new ArrayList<>();
        }
    }




    @Override
    public Integer updateCopiesBorrow(String copyId, String userId) {
        try{
            Example example = new Example(Copies.class);
            Example.Criteria criteria = example.createCriteria();
            if (!(("").equals(copyId) || null == copyId)) {
                criteria.andCondition(" copy_id = '" + copyId + "'");
            }
            List<Copies> copies = this.selectByExample(example);

            if(("1").equals(copies.get(0).getBorrowed()))
                return FAILED;
            else
                copiesMapper.borrowCopy(userId, copyId);
            return SUCCESS;
        }catch (Exception e) {
            log.error("Borrow exception!", e );
            return EXCEPTION;
        }

    }

    @Override
    public Integer updateCopiesReturn(String copyId) {

        try{
            Example example = new Example(Copies.class);
            Example.Criteria criteria = example.createCriteria();
            if (!(("").equals(copyId) || null == copyId)) {
                criteria.andCondition(" copy_id = '" + copyId + "'");
            }
            List<Copies> copies = this.selectByExample(example);

            if(("0").equals(copies.get(0).getBorrowed()))
                return FAILED;
            else
                copiesMapper.returnCopy(copyId);
            return SUCCESS;
        }catch (Exception e) {
            log.error("Borrow exception!", e );
            return EXCEPTION;
        }
    }

    public void addCopy(Copies copy) {
        this.save(copy);
    }

    public void updateCopy(Copies copy) {
        this.updateNotNull(copy);
    }

    public void deleteCopy(String copyIds) {
        List<String> list = Arrays.asList(copyIds.split(","));
        this.batchDelete(list, "copyId", Copies.class);
    }

    public List<Copies> findAll() {
        try {
            Example example = new Example(Copies.class);
            example.setOrderByClause("copy_id asc");
            return this.selectByExample(example);
        } catch (Exception e) {
            log.error("query failed!", e );
            return new ArrayList<>();
        }
    }
}
