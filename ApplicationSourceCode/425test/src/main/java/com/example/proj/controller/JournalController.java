package com.example.proj.controller;

import com.example.common.controller.BaseController;
import com.example.common.domain.ResponseResult;
import com.example.proj.pojo.Journal;
import com.example.proj.pojo.WrittenbyKey;
import com.example.proj.service.JournalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@CrossOrigin(origins = "*",maxAge = 3600)
@Controller
public class JournalController extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JournalService journalService;

    @RequestMapping("/journal/list")
    @ResponseBody
    public List getList() {
        return this.journalService.findAll();
    }

    @RequestMapping("journal/delete")
    @ResponseBody
    public ResponseResult deleteDocuments(String ids) {
        try {
            this.journalService.delete(ids);
            return ResponseResult.ok(" delete successfully!");
        } catch (Exception e) {
            log.error(" delete failed!", e);
            return  ResponseResult.error(" delete failed!");
        }
    }

    @RequestMapping("journal/add")
    @ResponseBody
    public ResponseResult addDocuments(Journal journal) {
        try {
            this.journalService.add(journal);
            return ResponseResult.ok(" add successfully!");
        } catch (Exception e) {
            log.error(" add failed!", e);
            return ResponseResult.error(" add failed!");
        }
    }

    @RequestMapping("journal/update")
    @ResponseBody
    public ResponseResult updateDocuments(Journal journal) {
        try {
            this.journalService.update(journal);
            return ResponseResult.ok(" update successfully!");
        } catch (Exception e) {
            log.error(" update failed!", e);
            return ResponseResult.error(" update failed!");
        }

    }
}
