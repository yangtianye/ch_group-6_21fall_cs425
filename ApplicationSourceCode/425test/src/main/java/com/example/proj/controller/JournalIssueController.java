package com.example.proj.controller;

import com.example.common.controller.BaseController;
import com.example.common.domain.ResponseResult;
import com.example.proj.pojo.Journalissue;
import com.example.proj.service.JournalIssueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@CrossOrigin(origins = "*",maxAge = 3600)
@Controller
public class JournalIssueController extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JournalIssueService journalIssueService;

    @RequestMapping("/ji/list")
    @ResponseBody
    public List getList() {
        return this.journalIssueService.findAll();
    }

    @RequestMapping("ji/delete")
    @ResponseBody
    public ResponseResult deleteDocuments(String ids) {
        try {
            this.journalIssueService.delete(ids);
            return ResponseResult.ok(" delete successfully!");
        } catch (Exception e) {
            log.error(" delete failed!", e);
            return  ResponseResult.error(" delete failed!");
        }
    }

    @RequestMapping("ji/add")
    @ResponseBody
    public ResponseResult addDocuments(Journalissue journalissue) {
        try {
            this.journalIssueService.add(journalissue);
            return ResponseResult.ok(" add successfully!");
        } catch (Exception e) {
            log.error(" add failed!", e);
            return ResponseResult.error(" add failed!");
        }
    }

    @RequestMapping("ji/update")
    @ResponseBody
    public ResponseResult updateDocuments(Journalissue journalissue) {
        try {
            this.journalIssueService.update(journalissue);
            return ResponseResult.ok(" update successfully!");
        } catch (Exception e) {
            log.error(" update failed!", e);
            return ResponseResult.error(" update failed!");
        }

    }
}
