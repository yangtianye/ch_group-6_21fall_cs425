package com.example.proj.dao;

import com.example.common.config.MyMapper;
import com.example.proj.pojo.Books;

public interface BooksMapper extends MyMapper<Books> {
}