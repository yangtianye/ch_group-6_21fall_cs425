package com.example.proj.service;

import com.example.common.service.IService;
import com.example.proj.pojo.DocumentdescriptionKey;

import java.util.List;

public interface DocumentsDescriptService extends IService<DocumentdescriptionKey> {

    List<DocumentdescriptionKey> findAll ();

    void update(DocumentdescriptionKey documentsDesctipt);

    void delete(String Ids);

    void add(DocumentdescriptionKey documentsDesctipt);
}
