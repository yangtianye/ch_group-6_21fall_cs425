package com.example.proj.dao;

import com.example.common.config.MyMapper;
import com.example.proj.pojo.Issue;

public interface IssueMapper extends MyMapper<Issue> {
}