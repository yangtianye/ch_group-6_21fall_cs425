package com.example.proj.pojo;

import javax.persistence.*;

public class Journal {
    @Id
    @Column(name = "journal_id")
    private String journalId;

    private String title;

    private String type;

    @Column(name = "issue_id")
    private String issueId;

    /**
     * @return journal_id
     */
    public String getJournalId() {
        return journalId;
    }

    /**
     * @param journalId
     */
    public void setJournalId(String journalId) {
        this.journalId = journalId == null ? null : journalId.trim();
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * @return issue_id
     */
    public String getIssueId() {
        return issueId;
    }

    /**
     * @param issueId
     */
    public void setIssueId(String issueId) {
        this.issueId = issueId == null ? null : issueId.trim();
    }
}