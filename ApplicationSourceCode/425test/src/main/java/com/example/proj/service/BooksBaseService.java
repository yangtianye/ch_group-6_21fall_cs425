package com.example.proj.service;

import com.example.common.service.IService;
import com.example.proj.pojo.BooksBase;
import com.example.proj.pojo.Documents;

import java.util.List;

public interface BooksBaseService extends IService<BooksBase> {
    List<BooksBase> findAll ();

    void update(BooksBase booksBase);

    void delete(String Ids);

    void add(BooksBase booksBase);
}
