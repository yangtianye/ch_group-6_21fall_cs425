package com.example.proj.service.impl;

import com.example.common.service.impl.BaseService;
import com.example.proj.pojo.Documents;
import com.example.proj.pojo.Users;
import com.example.proj.pojo.WrittenbyKey;
import com.example.proj.service.WrittenByService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("WrittenByServiceImpl")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class WrittenByServiceImpl extends BaseService<WrittenbyKey> implements WrittenByService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<WrittenbyKey> findAll() {
        try {
            Example example = new Example(WrittenbyKey.class);
            example.setOrderByClause("books_doc_id asc");
            return this.selectByExample(example);
        } catch (Exception e) {
            log.error("query failed", e );
            return new ArrayList<>();
        }
    }

    @Override
    public void update(WrittenbyKey writtenbyKey) {
        this.updateNotNull(writtenbyKey);
    }

    @Override
    public void delete(String Ids) {
        List<String> list = Arrays.asList(Ids.split(","));
        this.batchDelete(list, "booksDocId", WrittenbyKey.class);
    }

    @Override
    public void add(WrittenbyKey writtenbyKey) {
        this.save(writtenbyKey);
    }
}
