package com.example.proj.service.impl;

import com.example.common.service.impl.BaseService;
import com.example.proj.pojo.JournalArticles;
import com.example.proj.pojo.Magazines;
import com.example.proj.service.MagazinesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

@Service("MagazinesServiceImpl")
public class MagazinesServiceImpl extends BaseService<Magazines> implements MagazinesService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<Magazines> findByConditions(String editor, String title, String classification, String copyborrowed) {
        try {
            Example example = new Example(Magazines.class);
            Example.Criteria criteria = example.createCriteria();
            if (!(("").equals(editor) || null == editor))
                criteria.andCondition("editor = '" + editor + "'");
            if (!(("").equals(title) || null == title))
                criteria.andCondition("title = '" + title+ "'");
            if (!(("").equals(classification) || null == classification))
                criteria.andCondition("classification ='" + classification+ "'");
            if (!(("").equals(copyborrowed) || null == copyborrowed)) {
                if (("1").equals(copyborrowed))
                    criteria.andCondition("borrowed > 0 ");
            }
            List<Magazines> magazines = this.selectByExample(example);
            return magazines;
        }catch (Exception e) {
            log.error("query by conditions failed!", e );
            return new ArrayList<>();
        }
    }

    @Override
    public List<Magazines> havingKeywords(String keywords) {
        try{
            Example example = new Example(Magazines.class);
            Example.Criteria criteria = example.createCriteria();
            if (!(("").equals(keywords) || null == keywords))
                criteria.andCondition("title ilike '%"+keywords+"%' or keywords ilike '%"+keywords+"%' or classification ilike '%"+keywords+"%'");
            else
                return new ArrayList<>();
            List<Magazines> magazines = this.selectByExample(example);
            return magazines;
        }catch (Exception e) {
            log.error("query by conditions failed!", e );
            return new ArrayList<>();
        }
    }
}
