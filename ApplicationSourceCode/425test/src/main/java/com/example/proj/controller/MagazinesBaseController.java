package com.example.proj.controller;

import com.example.common.controller.BaseController;
import com.example.common.domain.ResponseResult;
import com.example.proj.pojo.MagazinesBase;
import com.example.proj.pojo.WrittenbyKey;
import com.example.proj.service.MagazinesBaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@CrossOrigin(origins = "*",maxAge = 3600)
@Controller
public class MagazinesBaseController extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MagazinesBaseService magazinesBaseService;

    @RequestMapping("/mb/list")
    @ResponseBody
    public List getList() {
        return this.magazinesBaseService.findAll();
    }

    @RequestMapping("mb/delete")
    @ResponseBody
    public ResponseResult deleteDocuments(String ids) {
        try {
            this.magazinesBaseService.delete(ids);
            return ResponseResult.ok(" delete successfully!");
        } catch (Exception e) {
            log.error(" delete failed!", e);
            return  ResponseResult.error(" delete failed!");
        }
    }

    @RequestMapping("mb/add")
    @ResponseBody
    public ResponseResult addDocuments(MagazinesBase magazinesBase) {
        try {
            this.magazinesBaseService.add(magazinesBase);
            return ResponseResult.ok(" add successfully!");
        } catch (Exception e) {
            log.error(" add failed!", e);
            return ResponseResult.error(" add failed!");
        }
    }

    @RequestMapping("mb/update")
    @ResponseBody
    public ResponseResult updateDocuments(MagazinesBase magazinesBase) {
        try {
            this.magazinesBaseService.update(magazinesBase);
            return ResponseResult.ok(" update successfully!");
        } catch (Exception e) {
            log.error(" update failed!", e);
            return ResponseResult.error(" update failed!");
        }

    }
}
