package com.example.proj.controller;

import com.example.common.controller.BaseController;
import com.example.common.domain.ResponseResult;
import com.example.proj.pojo.Documents;
import com.example.proj.pojo.WrittenbyKey;
import com.example.proj.service.WrittenByService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@CrossOrigin(origins = "*",maxAge = 3600)
@Controller
public class WrittenByController extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private WrittenByService writtenByService;


    @RequestMapping("/written/list")
    @ResponseBody
    public List getList() {
        return this.writtenByService.findAll();
    }

    @RequestMapping("written/delete")
    @ResponseBody
    public ResponseResult deleteDocuments(String ids) {
        try {
            this.writtenByService.delete(ids);
            return ResponseResult.ok(" delete successfully!");
        } catch (Exception e) {
            log.error(" delete failed!", e);
            return  ResponseResult.error(" delete failed!");
        }
    }

    @RequestMapping("written/add")
    @ResponseBody
    public ResponseResult addDocuments(WrittenbyKey writtenbyKey) {
        try {
            this.writtenByService.add(writtenbyKey);
            return ResponseResult.ok(" add successfully!");
        } catch (Exception e) {
            log.error(" add failed!", e);
            return ResponseResult.error(" add failed!");
        }
    }

    @RequestMapping("written/update")
    @ResponseBody
    public ResponseResult updateDocuments(WrittenbyKey writtenbyKey) {
        try {
            this.writtenByService.update(writtenbyKey);
            return ResponseResult.ok(" update successfully!");
        } catch (Exception e) {
            log.error(" update failed!", e);
            return ResponseResult.error(" update failed!");
        }

    }
}
