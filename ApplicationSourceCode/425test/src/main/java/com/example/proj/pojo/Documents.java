package com.example.proj.pojo;

import javax.persistence.*;

public class Documents {
    @Id
    @Column(name = "doc_id")
    private String docId;

    private String title;

    @Column(name = "user_id")
    private String userId;

    /**
     * @return doc_id
     */
    public String getDocId() {
        return docId;
    }

    /**
     * @param docId
     */
    public void setDocId(String docId) {
        this.docId = docId == null ? null : docId.trim();
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * @return user_id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }
}