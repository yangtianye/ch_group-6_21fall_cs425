package com.example.proj.pojo;

import java.util.Date;
import javax.persistence.*;

@Table(name = "magazines")
public class MagazinesBase {
    @Id
    @Column(name = "magazines_doc_id")
    private String magazinesDocId;

    private String name;

    @Column(name = "publish_time")
    private Date publishTime;

    @Column(name = "issue_id")
    private String issueId;

    /**
     * @return magazines_doc_id
     */
    public String getMagazinesDocId() {
        return magazinesDocId;
    }

    /**
     * @param magazinesDocId
     */
    public void setMagazinesDocId(String magazinesDocId) {
        this.magazinesDocId = magazinesDocId == null ? null : magazinesDocId.trim();
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * @return publish_time
     */
    public Date getPublishTime() {
        return publishTime;
    }

    /**
     * @param publishTime
     */
    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    /**
     * @return issue_id
     */
    public String getIssueId() {
        return issueId;
    }

    /**
     * @param issueId
     */
    public void setIssueId(String issueId) {
        this.issueId = issueId == null ? null : issueId.trim();
    }
}