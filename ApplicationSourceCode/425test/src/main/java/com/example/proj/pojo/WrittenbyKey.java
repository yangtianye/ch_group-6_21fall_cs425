package com.example.proj.pojo;

import javax.persistence.*;

@Table(name = "writtenby")
public class WrittenbyKey {
    @Id
    @Column(name = "books_doc_id")
    private String booksDocId;

    @Id
    @Column(name = "journal_articles_doc_id")
    private String journalArticlesDocId;

    @Id
    @Column(name = "author_name")
    private String authorName;

    /**
     * @return books_doc_id
     */
    public String getBooksDocId() {
        return booksDocId;
    }

    /**
     * @param booksDocId
     */
    public void setBooksDocId(String booksDocId) {
        this.booksDocId = booksDocId == null ? null : booksDocId.trim();
    }

    /**
     * @return journal_articles_doc_id
     */
    public String getJournalArticlesDocId() {
        return journalArticlesDocId;
    }

    /**
     * @param journalArticlesDocId
     */
    public void setJournalArticlesDocId(String journalArticlesDocId) {
        this.journalArticlesDocId = journalArticlesDocId == null ? null : journalArticlesDocId.trim();
    }

    /**
     * @return author_name
     */
    public String getAuthorName() {
        return authorName;
    }

    /**
     * @param authorName
     */
    public void setAuthorName(String authorName) {
        this.authorName = authorName == null ? null : authorName.trim();
    }
}