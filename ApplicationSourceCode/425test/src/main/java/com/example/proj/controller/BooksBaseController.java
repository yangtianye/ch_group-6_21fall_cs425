package com.example.proj.controller;

import com.example.common.controller.BaseController;
import com.example.common.domain.ResponseResult;
import com.example.proj.pojo.BooksBase;
import com.example.proj.pojo.Documents;
import com.example.proj.service.BooksBaseService;
import com.example.proj.service.DocumentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@CrossOrigin(origins = "*",maxAge = 3600)
@Controller
public class BooksBaseController extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BooksBaseService booksBaseService;

    @RequestMapping("/booksBase/list")
    @ResponseBody
    public List getList() {
        return this.booksBaseService.findAll();
    }

    @RequestMapping("booksBase/delete")
    @ResponseBody
    public ResponseResult deleteDocuments(String ids) {
        try {
            this.booksBaseService.delete(ids);
            return ResponseResult.ok("delete successfully!");
        } catch (Exception e) {
            log.error("delete failed!", e);
            return  ResponseResult.error("delete failed!");
        }
    }

    @RequestMapping("booksBase/add")
    @ResponseBody
    public ResponseResult addDocuments(BooksBase book) {
        try {
            this.booksBaseService.add(book);
            return ResponseResult.ok("add successfully!");
        } catch (Exception e) {
            log.error("add failed!", e);
            return ResponseResult.error("add failed!");
        }
    }

    @RequestMapping("booksBase/update")
    @ResponseBody
    public ResponseResult updateDocuments(BooksBase book) {
        try {
            this.booksBaseService.update(book);
            return ResponseResult.ok("update successfully!");
        } catch (Exception e) {
            log.error("update failed!", e);
            return ResponseResult.error("update failed!");
        }

    }
}
