package com.example.proj.service.impl;

import com.example.common.service.impl.BaseService;
import com.example.proj.pojo.Books;
import com.example.proj.service.BooksService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

@Service("BooksService")
public class BooksServiceImpl extends BaseService<Books> implements BooksService {

    private Logger log = LoggerFactory.getLogger(this.getClass());


    @Override
    public List<Books> findAll() {
        try {
            Example example = new Example(Books.class);
            example.setOrderByClause("books_doc_id asc");
            return this.selectByExample(example);
        } catch (Exception e) {
            log.error("query all failed!", e );
            return new ArrayList<>();
        }
    }

    public List<Books> findByConditions(String author, String title, String classification, String copyborrowed) {
        try {
            Example example = new Example(Books.class);
            Example.Criteria criterias = example.createCriteria();
            if (!(("").equals(author) || null == author))
                criterias.andCondition("author_name = '" + author + "'");
            if (!(("").equals(title) || null == title))
                criterias.andCondition("title = '" + title+ "'");
            if (!(("").equals(classification) || null == classification))
                criterias.andCondition("classification ='" + classification+ "'");
            if (!(("").equals(copyborrowed) || null == copyborrowed)) {
                if ("1".equals(copyborrowed))
                    criterias.andCondition("borrowed > 0 ");
                else
                    criterias.andCondition("borrowed = 0 ");
            }
            List<Books> books = this.selectByExample(example);
            return books;
        }catch (Exception e) {
            log.error("query by conditions failed!", e );
            return new ArrayList<>();
        }
    }

    @Override
    public List<Books> havingKeywords(String keywords) {
        try{
            Example example = new Example(Books.class);
            Example.Criteria criterias = example.createCriteria();
            if (!(("").equals(keywords) || null == keywords))
                criterias.andCondition("title ilike '%"+keywords+"%' or keywords ilike '%"+keywords+"%' or classification ilike '%"+keywords+"%'");
            else
                return new ArrayList<>();
            List<Books> books = this.selectByExample(example);
            return books;
        }catch (Exception e) {
            log.error("query by conditions failed!", e );
            return new ArrayList<>();
        }
    }
}
