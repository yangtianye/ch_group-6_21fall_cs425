package com.example.proj.controller;

import com.example.common.controller.BaseController;
import com.example.common.domain.ResponseResult;
import com.example.proj.pojo.Documents;
import com.example.proj.pojo.Users;
import com.example.proj.service.DocumentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@CrossOrigin(origins = "*",maxAge = 3600)
@Controller
public class DocumentsController extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DocumentsService documentsService;

    @RequestMapping("/documents/list")
    @ResponseBody
    public List getList() {
        return this.documentsService.findAll();
    }

    @RequestMapping("documents/delete")
    @ResponseBody
    public ResponseResult deleteDocuments(String ids) {
        try {
            this.documentsService.delete(ids);
            return ResponseResult.ok("Documents delete successfully!");
        } catch (Exception e) {
            log.error("Documents delete failed!", e);
            return  ResponseResult.error("Documents delete failed!");
        }
    }

    @RequestMapping("documents/add")
    @ResponseBody
    public ResponseResult addDocuments(Documents documents) {
        try {
            this.documentsService.add(documents);
            return ResponseResult.ok("Documents add successfully!");
        } catch (Exception e) {
            log.error("Documents add failed!", e);
            return ResponseResult.error("Documents add failed!");
        }
    }

    @RequestMapping("documents/update")
    @ResponseBody
    public ResponseResult updateDocuments(Documents documents) {
        try {
            this.documentsService.update(documents);
            return ResponseResult.ok("Documents update successfully!");
        } catch (Exception e) {
            log.error("Documents update failed!", e);
            return ResponseResult.error("Documents update failed!");
        }

    }
}
