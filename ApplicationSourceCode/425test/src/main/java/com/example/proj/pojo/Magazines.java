package com.example.proj.pojo;

import java.util.Date;
import javax.persistence.*;

@Table(name = "magazines_view")
public class Magazines {
    @Column(name = "magazines_doc_id")
    private String magazinesDocId;

    private String magzinename;

    private String title;

    @Column(name = "publish_time")
    private Date publishTime;

    private String editor;

    private String issutitle;

    private String types;

    private String keywords;

    private String classification;

    private Long borrowed;

    /**
     * @return magazines_doc_id
     */
    public String getMagazinesDocId() {
        return magazinesDocId;
    }

    /**
     * @param magazinesDocId
     */
    public void setMagazinesDocId(String magazinesDocId) {
        this.magazinesDocId = magazinesDocId == null ? null : magazinesDocId.trim();
    }

    /**
     * @return magzinename
     */
    public String getMagzinename() {
        return magzinename;
    }

    /**
     * @param magzinename
     */
    public void setMagzinename(String magzinename) {
        this.magzinename = magzinename == null ? null : magzinename.trim();
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * @return publish_time
     */
    public Date getPublishTime() {
        return publishTime;
    }

    /**
     * @param publishTime
     */
    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    /**
     * @return editor
     */
    public String getEditor() {
        return editor;
    }

    /**
     * @param editor
     */
    public void setEditor(String editor) {
        this.editor = editor == null ? null : editor.trim();
    }

    /**
     * @return issutitle
     */
    public String getIssutitle() {
        return issutitle;
    }

    /**
     * @param issutitle
     */
    public void setIssutitle(String issutitle) {
        this.issutitle = issutitle == null ? null : issutitle.trim();
    }

    /**
     * @return types
     */
    public String getTypes() {
        return types;
    }

    /**
     * @param types
     */
    public void setTypes(String types) {
        this.types = types == null ? null : types.trim();
    }

    /**
     * @return keywords
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * @param keywords
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    /**
     * @return classification
     */
    public String getClassification() {
        return classification;
    }

    /**
     * @param classification
     */
    public void setClassification(String classification) {
        this.classification = classification == null ? null : classification.trim();
    }

    /**
     * @return borrowed
     */
    public Long getBorrowed() {
        return borrowed;
    }

    /**
     * @param borrowed
     */
    public void setBorrowed(Long borrowed) {
        this.borrowed = borrowed;
    }
}