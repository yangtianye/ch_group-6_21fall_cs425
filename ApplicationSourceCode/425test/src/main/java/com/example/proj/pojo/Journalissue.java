package com.example.proj.pojo;

import java.util.Date;
import javax.persistence.*;

public class Journalissue {
    @Id
    @Column(name = "issue_id")
    private String issueId;

    private Date date;

    /**
     * @return issue_id
     */
    public String getIssueId() {
        return issueId;
    }

    /**
     * @param issueId
     */
    public void setIssueId(String issueId) {
        this.issueId = issueId == null ? null : issueId.trim();
    }

    /**
     * @return date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }
}