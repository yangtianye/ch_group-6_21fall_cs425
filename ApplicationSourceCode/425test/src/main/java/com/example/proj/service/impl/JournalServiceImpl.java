package com.example.proj.service.impl;

import com.example.common.service.impl.BaseService;
import com.example.proj.pojo.Journal;
import com.example.proj.pojo.WrittenbyKey;
import com.example.proj.service.JournalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("JournalServiceImpl")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class JournalServiceImpl extends BaseService<Journal> implements JournalService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<Journal> findAll() {
        try {
            Example example = new Example(Journal.class);
            example.setOrderByClause("journal_id asc");
            return this.selectByExample(example);
        } catch (Exception e) {
            log.error("query failed", e );
            return new ArrayList<>();
        }
    }

    @Override
    public void update(Journal journal) {
        this.updateNotNull(journal);
    }

    @Override
    public void delete(String Ids) {
        List<String> list = Arrays.asList(Ids.split(","));
        this.batchDelete(list, "journalId", Journal.class);

    }

    @Override
    public void add(Journal journal) {
        this.save(journal);
    }
}
