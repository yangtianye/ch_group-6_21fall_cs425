package com.example.proj.service.impl;

import com.example.common.service.impl.BaseService;
import com.example.proj.pojo.JournalArticlesBase;
import com.example.proj.pojo.WrittenbyKey;
import com.example.proj.service.JournalArticleBaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("JournalArticleBaseServiceImpl")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class JournalArticleBaseServiceImpl extends BaseService<JournalArticlesBase> implements JournalArticleBaseService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<JournalArticlesBase> findAll() {
        try {
            Example example = new Example(JournalArticlesBase.class);
            example.setOrderByClause("journal_articles_doc_id asc");
            return this.selectByExample(example);
        } catch (Exception e) {
            log.error("query failed", e );
            return new ArrayList<>();
        }
    }

    @Override
    public void update(JournalArticlesBase journalArticleBase) {
        this.updateNotNull(journalArticleBase);
    }

    @Override
    public void delete(String Ids) {
        List<String> list = Arrays.asList(Ids.split(","));
        this.batchDelete(list, "journalArticlesDocId", JournalArticlesBase.class);
    }

    @Override
    public void add(JournalArticlesBase journalArticleBase) {
        this.save(journalArticleBase);
    }
}
