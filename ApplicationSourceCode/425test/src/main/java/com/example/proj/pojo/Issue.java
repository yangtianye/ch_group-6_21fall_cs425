package com.example.proj.pojo;

import javax.persistence.*;

public class Issue {
    @Id
    @Column(name = "issue_id")
    private String issueId;

    /**
     * @return issue_id
     */
    public String getIssueId() {
        return issueId;
    }

    /**
     * @param issueId
     */
    public void setIssueId(String issueId) {
        this.issueId = issueId == null ? null : issueId.trim();
    }
}