import React, {FC} from 'react';
import {Button,Typography} from 'antd';
import './App.css';
import {Route, Routes, useNavigate, HashRouter} from 'react-router-dom';
import HomePage from "./Pages/HomePage";
import BorrowPage from "./Pages/BorrowPage";
import BookManagePage from "./Pages/BookManagePage";

const Introduction: React.FC = (props) => {
    let navigate = useNavigate();
    const { Title } = Typography;

    return (
        <div className="App">
            <Title>Group6 Project</Title>
            <Title level={2}>group members</Title>
            <Title level={5}>Sidi_Li (A20479715)</Title>
            <Title level={5}>Tianye_Yang (A20482352)</Title>
            <Title level={5}>Yan_Zhuang (A20479179)</Title>

            <Button type="primary" onClick={()=>{
                navigate('/home', {state: 'Members', replace: true});
            }} style={{margin:20}}>Enter as Members</Button>

            <Button type="primary" onClick={()=>{
                navigate('/home', {state: 'Librarians', replace: true});
            }} style={{margin:20}}>Enter as Librarians</Button>
        </div>
    )
}

const App: FC = () =>{
    return (
        <HashRouter>
            <Routes>
                <Route path="/" element={<Introduction />} />
                <Route path="/home" element={<HomePage />} />
                <Route path="/edit" element={<BookManagePage />} />
                <Route path="/borrow" element={<BorrowPage />} />
            </Routes>
        </HashRouter>
    );
}


export default App;