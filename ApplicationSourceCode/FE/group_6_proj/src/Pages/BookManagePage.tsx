import './BookManagePage.css';
import {Button, Input, Layout, Select, Typography} from "antd";
import {useLocation, useNavigate} from "react-router-dom";
import RequestUtils, {BookType, DocumentType} from "../Utils/RequestUtils";
import React, {useEffect, useState} from "react";

const { Option } = Select;
const { Title } = Typography;


const BookManagePage: React.FC = () => {
    let navigate = useNavigate();
    let location = useLocation();
    let docData = location.state["data"];
    let isUpdate = docData !== undefined;
    const [docType, setDocType] = useState(DocumentType.books);

    useEffect(()=>{
        if (docData !== undefined) {
            if (docData["journalArticlesDocId"] !== undefined) {
                setDocType(DocumentType.journalArticles)
            } else if(docData["magazinesDocId"]) {
                setDocType(DocumentType.magazines)
            } else {
                setDocType(DocumentType.books);
            }
        }
        // eslint-disable-next-line
    },[]);



    const exit = () => {
        // only librarians can access this page
        navigate('/home', {state: 'Librarians'});
    }

    const BookUpdateComponent: React.FC = () => {
        const [title, setTitle] = useState(docData ? docData["title"] : undefined);
        const [author, setAuthor] = useState(docData ? docData["authorName"] : undefined);
        const [classification, setClassification] = useState(docData ? docData["classification"] : undefined);
        const [types, setTypes] = useState(docData ? docData["types"] : undefined);
        const [keywords, setKeywords] = useState(docData ? docData["keywords"] : undefined);

        const saveClick = () => {
            if (isUpdate) {
                let reqData = docData
                reqData["title"] = title;
                reqData["authorName"] = author;
                reqData["classification"] = classification;
                reqData["types"] = types;
                reqData["keywords"] = keywords;
                RequestUtils.updateResource(BookType.document, {
                    docId: docData["booksDocId"],
                    title: title,
                }).then(res=>{
                    res.json().then(data => {
                        console.log(data);
                    })
                })
                // RequestUtils.updateResource(BookType.document, {
                //     docId: docData["booksDocId"],
                //     types: types,
                //     keywords: keywords,
                //     classification: classification
                // }).then(res=>{
                //     res.json().then(data => {
                //         console.log(data);
                //     })
                // })
                // RequestUtils.updateResource(BookType.written, {
                //     booksDocId: docData["booksDocId"],
                //     journal_articles_doc_id: "1",
                //     authorName: author
                // }).then(res=>{
                //     res.json().then(data => {
                //         console.log(data);
                //     })
                // })
            }else {
                RequestUtils.addResource(BookType.document, {
                    docId: 99,
                    title: title,
                }).then(res=>{
                    res.json().then(data => {
                        console.log(data);
                    })
                })
                RequestUtils.addResource(BookType.document, {
                    docId: 99,
                    types: types,
                    keywords: keywords,
                    classification: classification
                }).then(res=>{
                    res.json().then(data => {
                        console.log(data);
                    })
                })
                RequestUtils.addResource(BookType.written, {
                    booksDocId: 99,
                    authorName: author
                }).then(res=>{
                    res.json().then(data => {
                        console.log(data);
                    })
                })
            }
        }

        const layoutArr = [
            {
                title: 'Name',
                state: [title, setTitle],
            },
            // {
            //     title: 'Author',
            //     state: [author, setAuthor],
            // },
            // {
            //     title: 'Classification',
            //     state: [classification, setClassification],
            // },
            // {
            //     title: 'Types',
            //     state: [types, setTypes],
            // },
            // {
            //     title: 'Keywords',
            //     state: [keywords, setKeywords],
            // },
        ]

        const inputs = (l: any) => {
            let eArr: any[] = []
            l.forEach((dic: any) => {
                eArr.push(
                    <div>
                        <Title level={5}>{dic["title"]}</Title>
                        <Input className="book-manage-input"
                               placeholder={dic["title"]}
                               value={dic["state"][0]}
                               onChange={(e)=>{dic["state"][1](e.target.value)}}/>
                    </div>)
            })
            return eArr;
        }

        return (
            <div className="book-manage-main-container">
                {inputs(layoutArr)}
                <Button style={{width: 100}} type="primary" onClick={saveClick}>Save</Button>
            </div>
        )
    }

    const JAUpdateComponent: React.FC = () => {
        const [title, setTitle] = useState(docData ? docData["title"] : undefined);
        const [author, setAuthor] = useState(docData ? docData["authorName"] : undefined);
        const [classification, setClassification] = useState(docData ? docData["classification"] : undefined);
        const [jTitle, setJTitle] = useState(docData ? docData["journaltitle"] : undefined);
        const [keywords, setKeywords] = useState(docData ? docData["keywords"] : undefined);

        const saveClick = () => {
            // journalArticlesDocId
            // if (isUpdate) {
                // let reqData = docData
                // reqData["title"] = title;
                // reqData["authorName"] = author;
                // reqData["classification"] = classification;
                // reqData["types"] = types;
                // reqData["keywords"] = keywords;
                RequestUtils.updateResource(BookType.document, {
                    docId: docData["journalArticlesDocId"],
                    title: title,
                }).then(res=>{
                    res.json().then(data => {
                        console.log(data);
                    })
                })
                // RequestUtils.updateResource(BookType.document, {
                //     docId: docData["booksDocId"],
                //     types: types,
                //     keywords: keywords,
                //     classification: classification
                // }).then(res=>{
                //     res.json().then(data => {
                //         console.log(data);
                //     })
                // })
                // RequestUtils.updateResource(BookType.written, {
                //     booksDocId: docData["booksDocId"],
                //     authorName: author
                // }).then(res=>{
                //     res.json().then(data => {
                //         console.log(data);
                //     })
                // })
            // }else {
                // RequestUtils.addResource(BookType.document, {
                //     docId: 99,
                //     title: title,
                // }).then(res=>{
                //     res.json().then(data => {
                //         console.log(data);
                //     })
                // })
                // RequestUtils.addResource(BookType.document, {
                //     docId: 99,
                //     types: types,
                //     keywords: keywords,
                //     classification: classification
                // }).then(res=>{
                //     res.json().then(data => {
                //         console.log(data);
                //     })
                // })
                // RequestUtils.addResource(BookType.written, {
                //     booksDocId: 99,
                //     authorName: author
                // }).then(res=>{
                //     res.json().then(data => {
                //         console.log(data);
                //     })
                // })
            // }
        }

        const layoutArr = [
            {
                title: 'Name',
                state: [title, setTitle],
            },
            // {
            //     title: 'Author',
            //     state: [author, setAuthor],
            // },
            // {
            //     title: 'Classification',
            //     state: [classification, setClassification],
            // },
            // {
            //     title: 'Journal Title',
            //     state: [jTitle, setJTitle],
            // },
            // {
            //     title: 'Keywords',
            //     state: [keywords, setKeywords],
            // },
        ]

        const inputs = (l: any) => {
            let eArr: any[] = []
            l.forEach((dic: any) => {
                eArr.push(
                    <div>
                        <Title level={5}>{dic["title"]}</Title>
                        <Input className="book-manage-input"
                               placeholder={dic["title"]}
                               value={dic["state"][0]}
                               onChange={(e)=>{dic["state"][1](e.target.value)}}/>
                    </div>)
            })
            return eArr;
        }

        return (
            <div className="book-manage-main-container">
                {inputs(layoutArr)}
                <Button style={{width: 100}} type="primary" onClick={saveClick}>Save</Button>
            </div>
        )
    }

    const MagazineUpdateComponent: React.FC = () => {
        const [title, setTitle] = useState(docData ? docData["title"] : undefined);
        const [editor, setEditor] = useState(docData ? docData["editor"] : undefined);
        const [classification, setClassification] = useState(docData ? docData["classification"] : undefined);
        const [types, setTypes] = useState(docData ? docData["types"] : undefined);
        const [magzinename, setMagzinename] = useState(docData ? docData["magzinename"] : undefined);

        const saveClick = () => {
            // magazinesDocId
            // if (isUpdate) {
            //     let reqData = docData
            //     reqData["title"] = title;
            //     reqData["authorName"] = author;
            //     reqData["classification"] = classification;
            //     reqData["types"] = types;
            //     reqData["keywords"] = keywords;
                RequestUtils.updateResource(BookType.document, {
                    docId: docData["magazinesDocId"],
                    title: title,
                }).then(res=>{
                    res.json().then(data => {
                        console.log(data);
                    })
                })
            //     RequestUtils.updateResource(BookType.document, {
            //         docId: docData["booksDocId"],
            //         types: types,
            //         keywords: keywords,
            //         classification: classification
            //     }).then(res=>{
            //         res.json().then(data => {
            //             console.log(data);
            //         })
            //     })
            //     RequestUtils.updateResource(BookType.written, {
            //         booksDocId: docData["booksDocId"],
            //         authorName: author
            //     }).then(res=>{
            //         res.json().then(data => {
            //             console.log(data);
            //         })
            //     })
            // }else {
            //     RequestUtils.addResource(BookType.document, {
            //         docId: 99,
            //         title: title,
            //     }).then(res=>{
            //         res.json().then(data => {
            //             console.log(data);
            //         })
            //     })
            //     RequestUtils.addResource(BookType.document, {
            //         docId: 99,
            //         types: types,
            //         keywords: keywords,
            //         classification: classification
            //     }).then(res=>{
            //         res.json().then(data => {
            //             console.log(data);
            //         })
            //     })
            //     RequestUtils.addResource(BookType.written, {
            //         booksDocId: 99,
            //         authorName: author
            //     }).then(res=>{
            //         res.json().then(data => {
            //             console.log(data);
            //         })
            //     })
            // }
        }

        const layoutArr = [
            {
                title: 'Name',
                state: [title, setTitle],
            },
            // {
            //     title: 'Editor',
            //     state: [editor, setEditor],
            // },
            // {
            //     title: 'Classification',
            //     state: [classification, setClassification],
            // },
            // {
            //     title: 'Types',
            //     state: [types, setTypes],
            // },
            // {
            //     title: 'Magzine Name',
            //     state: [magzinename, setMagzinename],
            // },
        ]

        const inputs = (l: any) => {
            let eArr: any[] = []
            l.forEach((dic: any) => {
                eArr.push(
                    <div>
                        <Title level={5}>{dic["title"]}</Title>
                        <Input className="book-manage-input"
                               placeholder={dic["title"]}
                               value={dic["state"][0]}
                               onChange={(e)=>{dic["state"][1](e.target.value)}}/>
                    </div>)
            })
            return eArr;
        }

        return (
            <div className="book-manage-main-container">
                {inputs(layoutArr)}
                <Button style={{width: 100}} type="primary" onClick={saveClick}>Save</Button>
            </div>
        )
    }

    const contentComponent = () => {
        switch (docType) {
            case DocumentType.books:
                return (<BookUpdateComponent />)
            case DocumentType.journalArticles:
                return (<JAUpdateComponent />)
            case DocumentType.magazines:
                return (<MagazineUpdateComponent />)
        }
    }

    return (
        <Layout style={{padding:80}}>
            {!isUpdate ?
                <Select defaultValue={DocumentType.books} style={{width: 150}} onChange={(value)=>{
                    setDocType(value);
                }}>
                    <Option value={DocumentType.books}>Books</Option>
                    <Option value={DocumentType.journalArticles}>Journal Articles</Option>
                    <Option value={DocumentType.magazines}>Magazines</Option>
                </Select> : undefined
            }
            {contentComponent()}
            {isUpdate ? <Button style={{width: 100, marginTop: 20}} onClick={exit}>Exit</Button> : undefined}
        </Layout>
    )
}

export default BookManagePage;