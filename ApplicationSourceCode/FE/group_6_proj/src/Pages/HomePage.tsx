import React, {useState} from 'react';
import {Button, Checkbox, Collapse, Input, Layout, Menu, Select, Table} from "antd";
import RequestUtils, {DocumentType} from "../Utils/RequestUtils";
import {useLocation, useNavigate} from "react-router-dom";
import BookManagePage from "./BookManagePage";

const { Option } = Select;
const { Panel } = Collapse;
const { Header, Footer } = Layout;


const BookList: React.FC<{role: 'Members' | 'Librarians'}> = (props) => {
    const [queryType, setQueryType] = useState(DocumentType.books);
    const [copyData, setCopyData] = useState([]);
    const [keywords, setKeywords] = useState<string>();
    const [searchType, setSearchType] = useState<"keyword"|"conditional">("keyword");

    const [cAuthor, setCAuthor] = useState("");
    const [cTitle, setCTitle] = useState("");
    const [cClassification, setCClassification] = useState("");
    const [cCopyborrowed, setCCopyborrowed] = useState<1|0>(0);
    const [cQueryType, setCQueryType] = useState(DocumentType.books);
    const navigate = useNavigate();

    const getConditions = () => {
        return {
            pageNum: 1,
            pageSize: 20,
            author: cAuthor,
            title: cTitle,
            classification: cClassification,
            copyborrowed: cCopyborrowed,
            queryType: cQueryType,
        }
    }

    const getColumns = (type: DocumentType)=>{
        switch (type) {
            case DocumentType.books:
                return [
                    {
                        title:'Name',
                        dataIndex: 'title',
                        key:'title'
                    },
                    {
                        title: 'Author',
                        dataIndex: 'authorName',
                        key: 'authorName'
                    },
                    {
                        title: 'Classification',
                        dataIndex: 'classification',
                        key:'classification'
                    },
                    {
                        title: 'Types',
                        dataIndex: 'types',
                        key:'types'
                    },
                    {
                        title: 'Year',
                        dataIndex: 'year',
                        key:'year'
                    },
                    {
                        title: 'Keywords',
                        dataIndex: 'keywords',
                        key:'keywords'
                    },
                ]
            case DocumentType.journalArticles:
                return [
                    {
                        title:'Name',
                        dataIndex: 'title',
                        key:'title'
                    },
                    {
                        title: 'Author',
                        dataIndex: 'authorName',
                        key: 'authorName'
                    },
                    {
                        title: 'Classification',
                        dataIndex: 'classification',
                        key:'classification'
                    },
                    {
                        title: 'Journal Title',
                        dataIndex: 'journaltitle',
                        key: 'journaltitle'
                    },
                    {
                        title: 'Keywords',
                        dataIndex: 'keywords',
                        key:'keywords'
                    },
                ]
            case DocumentType.magazines:
                return [
                    {
                        title:'Name',
                        dataIndex: 'title',
                        key:'title'
                    },
                    {
                        title: 'Editor',
                        dataIndex: 'editor',
                        key: 'editor'
                    },
                    {
                        title: 'Classification',
                        dataIndex: 'classification',
                        key:'classification'
                    },
                    {
                        title: 'Types',
                        dataIndex: 'types',
                        key:'types'
                    },
                    {
                        title: 'Magzine Name',
                        dataIndex: 'magzinename',
                        key: 'magzinename'
                    },
                    {
                        title: 'Keywords',
                        dataIndex: 'keywords',
                        key:'keywords'
                    },
                ]
        }
    }
    const [columns, setColumns] = useState(getColumns(queryType));

    const tableStyle = {
        marginTop: 20
    }

    const searchClick = () => {
        switch (searchType) {
            case "keyword":
                queryKeywords();
                break;
            case "conditional":
                queryConditional();
                break;
        }
    }

    const queryConditional = () => {
        RequestUtils.queryResource(getConditions()).then(res => {
            res.json().then(data => {
                console.log(data.rows);
                setColumns(getColumns(cQueryType));
                setCopyData(addKeyToCopyData(data.rows));
            })
        })
    }

    const queryKeywords = () => {
        RequestUtils.queryKeywords({
            pageNum: 1,
            pageSize: 20,
            keywords: keywords ?? "",
            queryType: queryType
        }).then(res => {
            res.json().then(data => {
                console.log(data.rows);
                setColumns(getColumns(queryType));
                setCopyData(addKeyToCopyData(data.rows));
            })
        })
    }

    const addKeyToCopyData = (cData: any) => {
        for (const i in cData) {
            cData[i].key = i;
        }
        return cData
    }

    return (
        <div style={{margin: 80}}>
            <Collapse accordion defaultActiveKey={[1]} onChange={key => {
                switch (key) {
                    case '1':
                        setSearchType("keyword");
                        break;
                    case '2':
                        setSearchType("conditional");
                        break;
                }
            }}>
                <Panel key={1} header={"Keyword Search"}>
                    <div>
                        <Select defaultValue={DocumentType.books} style={{width: 150}} onChange={(value)=>{
                            setQueryType(value);
                        }}>
                            <Option value={DocumentType.books}>Books</Option>
                            <Option value={DocumentType.journalArticles}>Journal Articles</Option>
                            <Option value={DocumentType.magazines}>Magazines</Option>
                        </Select>
                        <Input placeholder="Keywords" onChange={(e)=>{
                            setKeywords(e.target.value);
                        }} style={{width: 300}} />
                    </div>
                </Panel>
                <Panel key={2} header={"Conditional Search"}>
                    <div>
                        <Select defaultValue={DocumentType.books} style={{width: 150}} onChange={(value)=>{
                            setCQueryType(value)
                        }}>
                            <Option value={DocumentType.books}>Books</Option>
                            <Option value={DocumentType.journalArticles}>Journal Articles</Option>
                            <Option value={DocumentType.magazines}>Magazines</Option>
                        </Select>
                        <Input placeholder="Author" onChange={(e)=>{
                            setCAuthor(e.target.value);
                        }} style={{marginTop: 10}} />
                        <Input placeholder="Title" onChange={(e)=>{
                            setCTitle(e.target.value);
                        }} style={{marginTop: 10}} />
                        <Input placeholder="Classification" onChange={(e)=>{
                            setCClassification(e.target.value);
                        }} style={{marginTop: 10}} />
                        <Checkbox style={{marginTop: 10}} onChange={(e) => {
                            setCCopyborrowed(e.target.checked ? 1 : 0)
                        }}>is borrowed</Checkbox>
                    </div>
                </Panel>
            </Collapse>
            <div>
                <Button style={{marginTop: 20}} onClick={searchClick}>Search</Button>
            </div>
            <div>


            </div>
            <Table
                dataSource={copyData}
                columns={columns}
                style={tableStyle}
                expandable={{
                    expandedRowRender: record => {
                        return (
                            <p style={{display: "flex", justifyContent: "space-around"}}>
                                {props.role === 'Librarians' ? <Button onClick={()=>{
                                    navigate('/edit', {state: {data: record}, replace: true});
                                }}>Edit</Button> : undefined}
                                <Button onClick={()=>{
                                    navigate('/borrow', {state: {data: record, role: props.role}, replace: true});
                                }}>Borrow/Return</Button>
                            </p>
                        )
                    },
                    rowExpandable: record => true,
                }}
            />
        </div>
    )
}
const HomePage: React.FC = () => {
    const [currentPage, setCurrentPage] = useState<'Home' | 'Manage'>('Home')
    let location = useLocation();
    let nav = useNavigate()
    let userRole: 'Members' | 'Librarians' = location.state;

    return (
        <Layout>
            <Header>
                <div className="logo" />
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                    <Menu.Item key={'1'} onClick={()=>setCurrentPage('Home')}>Main</Menu.Item>
                    {/*{userRole === 'Librarians' ? <Menu.Item key={'2'} onClick={()=>setCurrentPage('Manage')}>Manage</Menu.Item> : undefined}*/}
                    <Menu.Item key={'3'} onClick={()=>{nav('/')}}>Exit</Menu.Item>
                </Menu>
            </Header>
            {currentPage === 'Home' ? <BookList role={userRole}/> : <BookManagePage />}
            <Footer style={{ textAlign: 'center' }}>Group 6</Footer>
        </Layout>
    )
}


export default HomePage;