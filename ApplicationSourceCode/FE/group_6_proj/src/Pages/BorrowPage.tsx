import {Button, Input, Layout, Modal, Space, Table, Typography} from "antd";
import {useLocation, useNavigate} from "react-router-dom";
import React, {useEffect, useState} from "react";
import RequestUtils, {BookType} from "../Utils/RequestUtils";

const { Title } = Typography;

const BorrowPage: React.FC = () => {


    let navigate = useNavigate();
    let location = useLocation();
    let copyData = location.state["data"];
    let userRole: 'Members' | 'Librarians' = location.state["role"];
    const [copyList, setCopyList] = useState([]);
    const [copyId, setCopyId] = useState<string>();
    const [userId, setUserId] = useState<string>();
    const [isModalVisible, setIsModalVisible] = useState(false);

    const [newCopyId, setNewCopyId] = useState<string>();
    const [newLocation, setNewLocation] = useState<string>();

    const [isUpdate, setIsUpdate] = useState(false);

    const exit = () => {
        navigate('/home', {state: userRole});
    }

    const columns = [
        {
            title: 'ID',
            dataIndex: 'copyId',
            key:'copyId'
        },
        {
            title: 'Location',
            dataIndex: 'location',
            key:'location'
        },
    ]

    const deleteCopy = (copy: any) => {
        copy.key = undefined;
        RequestUtils.deleteResource(BookType.copy, copy).then(res => {
            queryCopy();
            res.json().then(data => {
                alert(data.msg);
            })
        });
    }

    const updateCopy = (copy: any) => {
        console.log(copy);
        setNewCopyId(copy.copyId);
        setNewLocation(copy.location);
        setIsUpdate(true);
        setIsModalVisible(true);
    }

    if (userRole === 'Librarians') {
        columns.push({
            title: 'Action',
            key: 'action',
            render: (text: any, record: any) => (
                <Space size="small">
                    <Button type='primary' onClick={()=>{updateCopy(record)}}>Update</Button>
                    <Button type='primary' onClick={()=>{deleteCopy(record)}}>Delete</Button>
                </Space>
            ),
        } as any)
    }

    const addKeyToCopyData = (cData: any) => {
        for (const i in cData) {
            cData[i].key = i;
        }
        return cData
    }

    const getDocID = () => {
        let docId = ""
        console.log(copyData);
        if (copyData["magazinesDocId"] !== undefined) {
            docId = copyData["magazinesDocId"];
        } else if (copyData["journalArticlesDocId"] !== undefined) {
            docId = copyData["journalArticlesDocId"];
        } else {
            docId = copyData["booksDocId"];
        }
        return docId;
    }

    const queryCopy = () => {
        let docId = getDocID();
        RequestUtils.checkCopyAvailable(docId).then(res => {
            res.json().then(data => {
                setCopyList(addKeyToCopyData(data.rows));
            })
        })
    }

    useEffect(()=>{
        queryCopy();
        // eslint-disable-next-line
    },[]);

    const checkCopyId = () => {
        if (copyId === undefined) {
            alert("The copy id is required!");
            return false;
        }
        return true
    }

    const checkUserId = () => {
        if (userId === undefined) {
            alert("The user id is required!");
            return false;
        }
        return true;
    }

    const clickBorrow = () => {
        if (checkCopyId() && checkUserId()) {
            RequestUtils.borrowCopy(copyId!, userId!).then(res => {
                queryCopy();
                res.json().then(data => {
                    alert(data.msg);
                });
            })
        }
    }

    const clickReturn = () => {
        if (checkCopyId()) {
            RequestUtils.returnCopy(copyId!).then(res => {
                queryCopy();
                res.json().then(data => {
                    alert(data.msg);
                });
            })
        }
    }

    const showAddCopy = () => {
        setIsModalVisible(true);
    }

    const handleAddCopy = () => {
        if (newCopyId === undefined) {
            alert("The copy id is required!");
            return;
        }
        if (newLocation === undefined) {
            alert("The location is required!");
            return;
        }
        let docId = getDocID();

        if (isUpdate) {
            RequestUtils.updateResource(BookType.copy, {
                copyId: newCopyId!,
                location: newLocation!,
                borrowed: 0,
                docId: docId
            }).then(res => {
                queryCopy();
                res.json().then(data => {
                    alert(data.msg);
                })
            })
            setIsUpdate(false);
        } else {
            RequestUtils.addResource(BookType.copy, {
                copyId: newCopyId!,
                location: newLocation!,
                borrowed: 0,
                docId: docId
            }).then(res => {
                queryCopy();
                res.json().then(data => {
                    alert(data.msg);
                })
            })
        }
        setNewCopyId(undefined);
        setNewLocation(undefined);
        setIsModalVisible(false);
    }

    const handleCancel = () => {
        setIsModalVisible(false);
        setNewCopyId(undefined);
        setNewLocation(undefined);
    }

    const roleElem = (element: any) => {
        return userRole === 'Librarians'? element:undefined
    }

    return (
        <Layout style={{padding: 80}}>
            <Title>{copyData["title"]}</Title>
            <Input placeholder="Copy ID" style={{marginTop:10}} onChange={(e)=>{
                setCopyId(e.target.value);
            }} />
            {roleElem(<Input placeholder="User ID" style={{marginTop:10}} onChange={(e)=>{
                setUserId(e.target.value);
            }} />)}
            <div style={{display:"flex", justifyContent:"space-around"}}>
                {roleElem(<Button onClick={clickBorrow} style={{marginTop:10}}>Borrow</Button>)}
                <Button onClick={clickReturn} style={{marginTop:10}}>Return</Button>
                {roleElem(<Button onClick={showAddCopy} style={{marginTop:10}}>Add Copy</Button>)}
                <Button onClick={exit} style={{marginTop:10}}>Exit</Button>
                <Modal title="Copy Information" visible={isModalVisible} onOk={handleAddCopy} onCancel={handleCancel}>
                    <Input placeholder="Copy ID"  style={{marginTop:10}} onChange={(e)=>{
                        setNewCopyId(e.target.value);
                    }} value={newCopyId} />
                    <Input placeholder="Location" style={{marginTop:10}} onChange={(e)=>{
                        setNewLocation(e.target.value);
                    }} value={newLocation} />
                </Modal>
            </div>
            <Table dataSource={copyList} columns={columns} style={{marginTop:10}} />
        </Layout>
    )
}

export default BorrowPage;