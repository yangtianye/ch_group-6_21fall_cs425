const host = 'http://localhost:8090';

export enum BookType {
    document = 'documents',
    documentsDes = 'documentsDes',
    booksBase = 'booksBase',
    written = 'written',
    magazines = 'mb',
    magazineIssue = 'mi',
    journal = 'journal',
    journalArticle = 'jab',
    journalIssue = 'ji',
    issue = 'issue',
    copy = 'copy',
}

export enum DocumentType {
    books = 'books',
    journalArticles = 'ja',
    magazines = 'maga'
}

export interface ResourcesQueryInterface {
    pageSize: number,
    pageNum: number,
    author?: string,
    title?: string,
    classification?: string,
    copyborrowed: 1 | 0,
    queryType: DocumentType,
}

export interface KeywordsQueryInterface {
    pageSize: number,
    pageNum: number,
    keywords: string,
    queryType: DocumentType,
}

function getReq(url: string, param: any) {
    if (param !== undefined) {
        let paramsArr: string[] = [];
        Object.keys(param).forEach(key => paramsArr.push(key + '=' + param[key]))
        if (url.search(/\?/) === -1) {
            url += '?' + paramsArr.join('&');
        } else {
            url += '&' + paramsArr.join('&');
        }
    }
    return fetch(url, {
        method: 'GET',
        mode: 'cors',
    })
}

function createForm(param: any) {
    if (param !== undefined) {
        let form = new FormData();
        Object.keys(param).forEach(key => {
            if (param[key] !== undefined && param[key] !== null ) {
                form.append(key, param[key]);
            }
        })
        return form;
    }
}

const RequestUtils = {
    copyDue: (userId: string) => {
        return getReq(host + '/copy/due', {userId: userId});
    },
    returnCopy: (copyId: string) => {
        return getReq(host + '/copy/return', {copyId: copyId});
    },
    borrowCopy: (copyId: string, userId: string) => {
        let param = {
            copyId: copyId,
            userId: userId
        }
        return getReq(host + '/copy/borrow', param);
    },
    checkCopyAvailable: (docId: string, pageSize?: number, pageNum?: number) => {
        let param = {
            docId: docId,
            pageSize: pageSize ?? 10,
            pageNum: pageNum ?? 1
        }
        return getReq(host + '/copy/query', param);
    },
    queryKeywords: (param: KeywordsQueryInterface) => {
        let t_params = {
            pageSize: param.pageSize,
            pageNum: param.pageNum,
            keywords: param.keywords,
        }
        return getReq(host + '/' + param.queryType + '/keywords', t_params);
    },
    queryResource: (param: ResourcesQueryInterface) => {
        let t_params: any = {
            pageSize: param.pageSize,
            pageNum: param.pageNum,
            title: param.title,
            classification: param.classification,
            copyborrowed: param.copyborrowed,
        }
        if (param.queryType === DocumentType.magazines) {
            t_params.editor = param.author
        }else {
            t_params.author = param.author
        }
        return getReq(host + '/'+ param.queryType +'/query', t_params);
    },
    getResourceList: (type: BookType) => {
        return fetch(host + '/' + type + '/list',{
            method: 'GET',
            mode: 'cors',
        })
    },
    addResource: (type: BookType, data: any) => {
        return fetch(host + '/' + type + '/add', {
            method: 'POST',
            mode: 'cors',
            body: createForm(data)
        })
    },
    updateResource: (type: BookType, data: any) => {
        return fetch(host + '/' + type + '/update', {
            method: 'POST',
            mode: 'cors',
            body: createForm(data)
        })
    },
    deleteResource: (type: BookType, data: any) => {
        return fetch(host + '/' + type + '/delete', {
            method: 'POST',
            mode: 'cors',
            body: createForm(data)
        })
    },
}

export default RequestUtils;