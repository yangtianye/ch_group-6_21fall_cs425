package com.example.common.config;

import tk.mybatis.mapper.common.Mapper;

public interface MyMapper<T> extends Mapper<T> {
}
