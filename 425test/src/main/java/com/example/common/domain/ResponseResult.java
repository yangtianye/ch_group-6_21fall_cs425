package com.example.common.domain;

import java.util.HashMap;

public class ResponseResult extends HashMap<String, Object> {

    private static final long serialVersionUID = -8713837118340960775L;

    // 成功
    private static final Integer SUCCESS = 0;
    // 警告
    private static final Integer WARN = 1;
    // 异常 失败
    private static final Integer FAIL = 500;

    public ResponseResult() {
        put("code", SUCCESS);
        put("msg", "操作成功");
    }

    public static ResponseResult error(Object msg) {
        ResponseResult responseBo = new ResponseResult();
        responseBo.put("code", FAIL);
        responseBo.put("msg", msg);
        return responseBo;
    }

    public static ResponseResult warn(Object msg) {
        ResponseResult responseBo = new ResponseResult();
        responseBo.put("code", WARN);
        responseBo.put("msg", msg);
        return responseBo;
    }

    public static ResponseResult ok(Object msg) {
        ResponseResult responseBo = new ResponseResult();
        responseBo.put("code", SUCCESS);
        responseBo.put("msg", msg);
        return responseBo;
    }

    public static ResponseResult ok() {
        return new ResponseResult();
    }

    public static ResponseResult error() {
        return ResponseResult.error("");
    }

    @Override
    public ResponseResult put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
