package com.example.proj.service.impl;

import com.example.common.service.impl.BaseService;
import com.example.proj.pojo.Documents;
import com.example.proj.service.DocumentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("DocumentsServiceImpl")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class DocumentsServiceImpl extends BaseService<Documents> implements DocumentsService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<Documents> findAll() {
        try {
            Example example = new Example(Documents.class);
            example.setOrderByClause("doc_id asc");
            return this.selectByExample(example);
        } catch (Exception e) {
            log.error("query failed!", e );
            return new ArrayList<>();
        }
    }

    @Override
    public void update(Documents documents) {
        this.updateNotNull(documents);
    }

    @Override
    public void delete(String Ids) {
        List<String> list = Arrays.asList(Ids.split(","));
        this.batchDelete(list, "docId", Documents.class);
    }

    @Override
    public void add(Documents documents) {
        this.save(documents);
    }
}
