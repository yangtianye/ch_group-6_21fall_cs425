package com.example.proj.service.impl;


import com.example.common.service.impl.BaseService;
import com.example.proj.pojo.MagazineIssue;
import com.example.proj.service.MagazineIssueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("MagazineIssueImpl")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class MagazineIssueServiceImpl extends BaseService<MagazineIssue> implements MagazineIssueService{

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<MagazineIssue> findAll() {
        try {
            Example example = new Example(MagazineIssue.class);
            example.setOrderByClause("issue_id asc");
            return this.selectByExample(example);
        } catch (Exception e) {
            log.error("query failed", e );
            return new ArrayList<>();
        }
    }

    @Override
    public void update(MagazineIssue magazineIssue) {
        this.updateNotNull(magazineIssue);
    }

    @Override
    public void delete(String Ids) {
        List<String> list = Arrays.asList(Ids.split(","));
        this.batchDelete(list, "issueId", MagazineIssue.class);
    }

    @Override
    public void add(MagazineIssue magazineIssue) {
        this.save(magazineIssue);
    }
}
