package com.example.proj.service.impl;

import com.example.common.service.impl.BaseService;
import com.example.proj.pojo.BooksBase;
import com.example.proj.pojo.Documents;
import com.example.proj.service.BooksBaseService;
import com.example.proj.service.BooksService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("BooksBaseServiceImpl")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BooksBaseServiceImpl extends BaseService<BooksBase> implements BooksBaseService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<BooksBase> findAll() {
        try {
            Example example = new Example(BooksBase.class);
            example.setOrderByClause("books_doc_id asc");
            return this.selectByExample(example);
        } catch (Exception e) {
            log.error("query failed!", e );
            return new ArrayList<>();
        }
    }

    @Override
    public void update(BooksBase booksBase) {
        this.updateNotNull(booksBase);
    }

    @Override
    public void delete(String Ids) {
        List<String> list = Arrays.asList(Ids.split(","));
        this.batchDelete(list, "booksDocId", BooksBase.class);
    }

    @Override
    public void add(BooksBase booksBase) {
        this.save(booksBase);
    }
}
