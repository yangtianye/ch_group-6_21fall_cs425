package com.example.proj.service;

import com.example.common.service.IService;
import com.example.proj.pojo.JournalArticles;

import java.util.List;

public interface JournalArticlesService extends IService<JournalArticles> {

    List<JournalArticles> findByConditions(String author, String title, String classification, String copyborrowed);

    List<JournalArticles> havingKeywords(String keywords);
}
