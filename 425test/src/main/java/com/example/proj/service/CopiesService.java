package com.example.proj.service;

import com.example.proj.pojo.Copies;

import java.util.List;

public interface CopiesService {

    List<Copies> havingCopies(String docId);

    List<Copies> userDue(String userId);

    Integer updateCopiesBorrow(String CopyId, String userId);

    Integer updateCopiesReturn(String CopyId);

    void addCopy(Copies copy);

    void updateCopy(Copies copy);

    void deleteCopy(String copyIds);

    List<Copies> findAll();
}
