package com.example.proj.service;

import com.example.common.service.IService;
import com.example.proj.pojo.Documents;

import java.util.List;

public interface DocumentsService extends IService<Documents> {
    List<Documents> findAll ();

    void update(Documents documents);

    void delete(String Ids);

    void add(Documents documents);
}
