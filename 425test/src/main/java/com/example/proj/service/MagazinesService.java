package com.example.proj.service;

import com.example.proj.pojo.Magazines;

import java.util.List;

public interface MagazinesService {

    List<Magazines> findByConditions(String editor, String title, String classification, String copyborrowed);

    List<Magazines> havingKeywords(String keywords);
}
