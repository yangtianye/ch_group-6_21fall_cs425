package com.example.proj.service.impl;

import com.example.common.service.impl.BaseService;
import com.example.proj.pojo.Issue;
import com.example.proj.service.IssueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("IssueServiceImpl")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class IssueServiceImpl extends BaseService<Issue> implements IssueService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<Issue> findAll() {
        try {
            Example example = new Example(Issue.class);
            example.setOrderByClause("issue_id asc");
            return this.selectByExample(example);
        } catch (Exception e) {
            log.error("query failed", e );
            return new ArrayList<>();
        }
    }

    @Override
    public void update(Issue issue) {
        this.updateNotNull(issue);
    }

    @Override
    public void delete(String Ids) {
        List<String> list = Arrays.asList(Ids.split(","));
        this.batchDelete(list, "issue_id", Issue.class);
    }

    @Override
    public void add(Issue issue) {
        this.save(issue);
    }
}
