package com.example.proj.service;

import com.example.common.service.IService;
import com.example.proj.pojo.Journalissue;

import java.util.List;

public interface JournalIssueService extends IService<Journalissue> {
    List<Journalissue> findAll ();

    void update(Journalissue journalissue);

    void delete(String Ids);

    void add(Journalissue journalissue);
}
