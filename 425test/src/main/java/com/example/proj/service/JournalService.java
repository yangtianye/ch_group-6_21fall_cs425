package com.example.proj.service;

import com.example.common.service.IService;
import com.example.proj.pojo.Journal;

import java.util.List;

public interface JournalService extends IService<Journal> {
    List<Journal> findAll ();

    void update(Journal journal);

    void delete(String Ids);

    void add(Journal journal);
}
