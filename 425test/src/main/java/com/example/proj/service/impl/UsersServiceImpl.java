package com.example.proj.service.impl;

import com.example.common.service.impl.BaseService;
import com.example.proj.pojo.Users;
import com.example.proj.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

@Service("UsersService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UsersServiceImpl extends BaseService<Users> implements UsersService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<Users> findAll(Users users) {
        try {
            Example example = new Example(Users.class);
            example.setOrderByClause("user_id asc");
            return this.selectByExample(example);
        } catch (Exception e) {
            log.error("query failed", e );
            return new ArrayList<>();
        }
    }


    @Override
    public Users findById(Long id) {
        return null;
    }

    @Override
    public void update(Users users) {

    }

    @Override
    public void delete(String Ids) {

    }

    @Override
    public void add(Users users) {

    }
}
