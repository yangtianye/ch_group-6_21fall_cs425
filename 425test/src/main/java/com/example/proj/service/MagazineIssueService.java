package com.example.proj.service;

import com.example.common.service.IService;
import com.example.proj.pojo.MagazineIssue;

import java.util.List;

public interface MagazineIssueService extends IService<MagazineIssue> {
    List<MagazineIssue> findAll ();

    void update(MagazineIssue magazineIssue);

    void delete(String Ids);

    void add(MagazineIssue magazineIssue);
}
