package com.example.proj.service;

import com.example.common.service.IService;
import com.example.proj.pojo.MagazinesBase;

import java.util.List;

public interface MagazinesBaseService extends IService<MagazinesBase> {
    List<MagazinesBase> findAll ();

    void update(MagazinesBase magazinesBase);

    void delete(String Ids);

    void add(MagazinesBase magazinesBase);
}
