package com.example.proj.controller;

import com.example.common.controller.BaseController;
import com.example.common.domain.QueryRequest;
import com.example.common.domain.ResponseResult;
import com.example.proj.pojo.Users;
import com.example.proj.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;


@CrossOrigin(origins = "*",maxAge = 3600)
@Controller
public class UsersController extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UsersService usersService;

    @RequestMapping("/test")
    @ResponseBody
    public List getList(Users users) {
        return this.usersService.findAll(users);
    }

    @RequestMapping("users/list")
    @ResponseBody
    public Map<String, Object> findAll(QueryRequest request, Users users) {
        return super.selectByPageNumSize(request, () -> this.usersService.findAll(users));
    }

    @RequestMapping("users/delete")
    @ResponseBody
    public ResponseResult deleteUsers(String ids) {
        try {
            this.usersService.delete(ids);
            return ResponseResult.ok("Users delete successfully!");
        } catch (Exception e) {
            log.error("Users delete failed!", e);
            return  ResponseResult.error("Users delete failed!");
        }
    }

    @RequestMapping("users/add")
    @ResponseBody
    public ResponseResult addUsers(Users users) {
        try {
            this.usersService.add(users);
            return ResponseResult.ok("Users add successfully!");
        } catch (Exception e) {
            log.error("Users add failed!", e);
            return ResponseResult.error("Users add failed!");
        }
    }

    @RequestMapping("users/update")
    @ResponseBody
    public ResponseResult updateUsers(Users users) {
        try {
            this.usersService.update(users);
            return ResponseResult.ok("Users update successfully!");
        } catch (Exception e) {
            log.error("Users update failed!", e);
            return ResponseResult.error("Users update failed!");
        }

    }



}
