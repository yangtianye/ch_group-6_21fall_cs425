package com.example.proj.controller;

import com.example.common.controller.BaseController;
import com.example.common.domain.QueryRequest;
import com.example.common.domain.ResponseResult;
import com.example.proj.pojo.Copies;
import com.example.proj.service.CopiesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*",maxAge = 3600)
@Controller
public class CopiesController extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CopiesService copiesService;

    @RequestMapping("copy/list")
    @ResponseBody
    public Map<String, Object> findAll(QueryRequest request) {
        return super.selectByPageNumSize(request,
                () -> this.copiesService.findAll());
    }

    @RequestMapping("copy/query")
    @ResponseBody
    public Map<String, Object> havingCopies(QueryRequest request, String docId) {
        return super.selectByPageNumSize(request,
                () -> this.copiesService.havingCopies(docId));
    }

    @RequestMapping("copy/due")
    @ResponseBody
    public List<Copies> copyDue(String userId) {
        return this.copiesService.userDue(userId);
    }

    @RequestMapping("copy/borrow")
    @ResponseBody
    public ResponseResult borrowCopy(String copyId, String userId) {
        try {
            Integer res = this.copiesService.updateCopiesBorrow(copyId, userId);
            if (res == 0)
                return ResponseResult.ok("Borrow successfully!");
            else if (res == 1)
                return ResponseResult.warn("Already Borrowed!");
            else
                return ResponseResult.error("Borrow failed!");
        } catch (Exception e) {
            log.error("Borrow failed!", e);
            return ResponseResult.error("Borrow failed!");
        }
    }

    @RequestMapping("copy/return")
    @ResponseBody
    public ResponseResult returnCopy(String copyId) {
        try {
            Integer res = this.copiesService.updateCopiesReturn(copyId);
            if (res == 0)
                return ResponseResult.ok("Return successfully!");
            else if (res == 1)
                return ResponseResult.warn("Already Returned!");
            else
                return ResponseResult.error("Returned failed!");

        } catch (Exception e) {
            log.error("Return failed!", e);
            return ResponseResult.error("Return failed!");
        }
    }

    @RequestMapping("copy/add")
    @ResponseBody
    public ResponseResult addCopies(Copies copies) {
        try {
            this.copiesService.addCopy(copies);
            return ResponseResult.ok("copy add successfully!");
        } catch (Exception e) {
            log.error("copy add failed!", e);
            return ResponseResult.error("copy add failed!");
        }
    }

    @RequestMapping("copy/update")
    @ResponseBody
    public ResponseResult updateCopies(Copies copies) {
        try {
            this.copiesService.updateCopy(copies);
            return ResponseResult.ok("copy update successfully!");
        } catch (Exception e) {
            log.error("copy update failed!", e);
            return ResponseResult.error("copy update failed!");
        }
    }

    @RequestMapping("/copy/delete")
    @ResponseBody
    public ResponseResult deleteCopies(Copies copies) {
        try {
            this.copiesService.deleteCopy(copies.getCopyId());
            return ResponseResult.ok("copy delete successfully!");
        } catch (Exception e) {
            log.error("copy delete failed!", e);
            return ResponseResult.error("copy delete failed!");
        }
    }


}
