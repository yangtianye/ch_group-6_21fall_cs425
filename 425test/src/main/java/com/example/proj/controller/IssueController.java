package com.example.proj.controller;

import com.example.common.controller.BaseController;
import com.example.common.domain.ResponseResult;
import com.example.proj.pojo.Issue;
import com.example.proj.pojo.WrittenbyKey;
import com.example.proj.service.IssueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@CrossOrigin(origins = "*",maxAge = 3600)
@Controller
public class IssueController extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IssueService issueService;

    @RequestMapping("/issue/list")
    @ResponseBody
    public List getList() {
        return this.issueService.findAll();
    }

    @RequestMapping("issue/delete")
    @ResponseBody
    public ResponseResult deleteDocuments(String ids) {
        try {
            this.issueService.delete(ids);
            return ResponseResult.ok(" delete successfully!");
        } catch (Exception e) {
            log.error(" delete failed!", e);
            return  ResponseResult.error(" delete failed!");
        }
    }

    @RequestMapping("issue/add")
    @ResponseBody
    public ResponseResult addDocuments(Issue issue) {
        try {
            this.issueService.add(issue);
            return ResponseResult.ok(" add successfully!");
        } catch (Exception e) {
            log.error(" add failed!", e);
            return ResponseResult.error(" add failed!");
        }
    }

    @RequestMapping("issue/update")
    @ResponseBody
    public ResponseResult updateDocuments(Issue issue) {
        try {
            this.issueService.update(issue);
            return ResponseResult.ok(" update successfully!");
        } catch (Exception e) {
            log.error(" update failed!", e);
            return ResponseResult.error(" update failed!");
        }

    }
}
