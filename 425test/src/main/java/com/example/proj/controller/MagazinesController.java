package com.example.proj.controller;

import com.example.common.controller.BaseController;
import com.example.common.domain.QueryRequest;
import com.example.proj.service.MagazinesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


@CrossOrigin(origins = "*",maxAge = 3600)
@Controller
public class MagazinesController extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MagazinesService magazinesService;

    @RequestMapping("maga/query")
    @ResponseBody
    public Map<String, Object> queryBooksInfo(QueryRequest request, String editor, String title, String classification, String copyborrowed) {
        return super.selectByPageNumSize(request,
                () -> this.magazinesService.findByConditions(editor, title, classification, copyborrowed));
    }

    @RequestMapping("maga/keywords")
    @ResponseBody
    public Map<String, Object> havingKeywords(QueryRequest request, String keywords) {
        return super.selectByPageNumSize(request,
                () -> this.magazinesService.havingKeywords(keywords));
    }
}
