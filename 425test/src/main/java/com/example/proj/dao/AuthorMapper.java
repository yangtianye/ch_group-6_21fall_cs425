package com.example.proj.dao;

import com.example.common.config.MyMapper;
import com.example.proj.pojo.Author;

public interface AuthorMapper extends MyMapper<Author> {
}