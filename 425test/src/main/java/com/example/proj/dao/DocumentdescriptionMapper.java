package com.example.proj.dao;

import com.example.common.config.MyMapper;
import com.example.proj.pojo.DocumentdescriptionKey;

public interface DocumentdescriptionMapper extends MyMapper<DocumentdescriptionKey> {
}