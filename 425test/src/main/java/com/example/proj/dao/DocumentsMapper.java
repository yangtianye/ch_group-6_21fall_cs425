package com.example.proj.dao;

import com.example.common.config.MyMapper;
import com.example.proj.pojo.Documents;

public interface DocumentsMapper extends MyMapper<Documents> {
}