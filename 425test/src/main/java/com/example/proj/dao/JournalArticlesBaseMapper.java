package com.example.proj.dao;

import com.example.common.config.MyMapper;
import com.example.proj.pojo.JournalArticlesBase;

public interface JournalArticlesBaseMapper extends MyMapper<JournalArticlesBase> {
}