package com.example.proj.dao;

import com.example.common.config.MyMapper;
import com.example.proj.pojo.Copies;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CopiesMapper extends MyMapper<Copies> {
    void borrowCopy(@Param("userId") String userId, @Param("copyId") String copyId);
    void returnCopy(String copyId);
}