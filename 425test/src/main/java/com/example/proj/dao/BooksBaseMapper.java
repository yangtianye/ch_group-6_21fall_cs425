package com.example.proj.dao;

import com.example.common.config.MyMapper;
import com.example.proj.pojo.BooksBase;

public interface BooksBaseMapper extends MyMapper<BooksBase> {
}