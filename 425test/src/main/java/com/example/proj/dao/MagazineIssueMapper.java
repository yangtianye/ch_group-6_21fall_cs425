package com.example.proj.dao;

import com.example.common.config.MyMapper;
import com.example.proj.pojo.MagazineIssue;

public interface MagazineIssueMapper extends MyMapper<MagazineIssue> {
}