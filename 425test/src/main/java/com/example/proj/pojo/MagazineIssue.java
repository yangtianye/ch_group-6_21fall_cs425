package com.example.proj.pojo;

import javax.persistence.*;

public class MagazineIssue {
    @Id
    @Column(name = "issue_id")
    private String issueId;

    private String title;

    /**
     * @return issue_id
     */
    public String getIssueId() {
        return issueId;
    }

    /**
     * @param issueId
     */
    public void setIssueId(String issueId) {
        this.issueId = issueId == null ? null : issueId.trim();
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }
}