package com.example.proj.pojo;

import javax.persistence.*;

@Table(name = "journalarticles_view")
public class JournalArticles {
    @Column(name = "journal_articles_doc_id")
    private String journalArticlesDocId;

    private String title;

    @Column(name = "journal_id")
    private String journalId;

    private String journaltitle;

    private String journaltype;

    @Column(name = "issue_id")
    private String issueId;

    @Column(name = "author_name")
    private String authorName;

    private String types;

    private String keywords;

    private String classification;

    private Long borrowed;

    /**
     * @return journal_articles_doc_id
     */
    public String getJournalArticlesDocId() {
        return journalArticlesDocId;
    }

    /**
     * @param journalArticlesDocId
     */
    public void setJournalArticlesDocId(String journalArticlesDocId) {
        this.journalArticlesDocId = journalArticlesDocId == null ? null : journalArticlesDocId.trim();
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * @return journal_id
     */
    public String getJournalId() {
        return journalId;
    }

    /**
     * @param journalId
     */
    public void setJournalId(String journalId) {
        this.journalId = journalId == null ? null : journalId.trim();
    }

    /**
     * @return journaltitle
     */
    public String getJournaltitle() {
        return journaltitle;
    }

    /**
     * @param journaltitle
     */
    public void setJournaltitle(String journaltitle) {
        this.journaltitle = journaltitle == null ? null : journaltitle.trim();
    }

    /**
     * @return journaltype
     */
    public String getJournaltype() {
        return journaltype;
    }

    /**
     * @param journaltype
     */
    public void setJournaltype(String journaltype) {
        this.journaltype = journaltype == null ? null : journaltype.trim();
    }

    /**
     * @return issue_id
     */
    public String getIssueId() {
        return issueId;
    }

    /**
     * @param issueId
     */
    public void setIssueId(String issueId) {
        this.issueId = issueId == null ? null : issueId.trim();
    }

    /**
     * @return author_name
     */
    public String getAuthorName() {
        return authorName;
    }

    /**
     * @param authorName
     */
    public void setAuthorName(String authorName) {
        this.authorName = authorName == null ? null : authorName.trim();
    }

    /**
     * @return types
     */
    public String getTypes() {
        return types;
    }

    /**
     * @param types
     */
    public void setTypes(String types) {
        this.types = types == null ? null : types.trim();
    }

    /**
     * @return keywords
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * @param keywords
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    /**
     * @return classification
     */
    public String getClassification() {
        return classification;
    }

    /**
     * @param classification
     */
    public void setClassification(String classification) {
        this.classification = classification == null ? null : classification.trim();
    }

    /**
     * @return borrowed
     */
    public Long getBorrowed() {
        return borrowed;
    }

    /**
     * @param borrowed
     */
    public void setBorrowed(Long borrowed) {
        this.borrowed = borrowed;
    }
}