package com.example.proj.pojo;

import javax.persistence.*;

@Table(name = "documentdescription")
public class DocumentdescriptionKey {
    @Id
    private String types;

    @Id
    private String keywords;

    @Id
    private String classification;

    @Id
    @Column(name = "doc_id")
    private String docId;

    /**
     * @return types
     */
    public String getTypes() {
        return types;
    }

    /**
     * @param types
     */
    public void setTypes(String types) {
        this.types = types == null ? null : types.trim();
    }

    /**
     * @return keywords
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * @param keywords
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    /**
     * @return classification
     */
    public String getClassification() {
        return classification;
    }

    /**
     * @param classification
     */
    public void setClassification(String classification) {
        this.classification = classification == null ? null : classification.trim();
    }

    /**
     * @return doc_id
     */
    public String getDocId() {
        return docId;
    }

    /**
     * @param docId
     */
    public void setDocId(String docId) {
        this.docId = docId == null ? null : docId.trim();
    }
}