package com.example.proj.pojo;

import javax.persistence.*;

@Table(name = "journalarticles")
public class JournalArticlesBase {
    @Id
    @Column(name = "journal_articles_doc_id")
    private String journalArticlesDocId;

    @Column(name = "journal_id")
    private String journalId;

    /**
     * @return journal_articles_doc_id
     */
    public String getJournalArticlesDocId() {
        return journalArticlesDocId;
    }

    /**
     * @param journalArticlesDocId
     */
    public void setJournalArticlesDocId(String journalArticlesDocId) {
        this.journalArticlesDocId = journalArticlesDocId == null ? null : journalArticlesDocId.trim();
    }

    /**
     * @return journal_id
     */
    public String getJournalId() {
        return journalId;
    }

    /**
     * @param journalId
     */
    public void setJournalId(String journalId) {
        this.journalId = journalId == null ? null : journalId.trim();
    }
}