
INSERT INTO public.users (user_id, role) VALUES ('005', 'librarians');
INSERT INTO public.users (user_id, role) VALUES ('006', 'members');
INSERT INTO public.users (user_id, role) VALUES ('007', 'members');
INSERT INTO public.users (user_id, role) VALUES ('008', 'librarians');
INSERT INTO public.users (user_id, role) VALUES ('009', 'members');
INSERT INTO public.users (user_id, role) VALUES ('010', 'members');
INSERT INTO public.users (user_id, role) VALUES ('011', 'members');
INSERT INTO public.users (user_id, role) VALUES ('012', 'members');
INSERT INTO public.users (user_id, role) VALUES ('001', 'libarians');
INSERT INTO public.users (user_id, role) VALUES ('002', 'libarians');
INSERT INTO public.users (user_id, role) VALUES ('003', 'members');
INSERT INTO public.users (user_id, role) VALUES ('004', 'members');


INSERT INTO public.members (members_user_id, borrow_num) VALUES ('003', '4');
INSERT INTO public.members (members_user_id, borrow_num) VALUES ('004', '0');
INSERT INTO public.members (members_user_id, borrow_num) VALUES ('006', '0');
INSERT INTO public.members (members_user_id, borrow_num) VALUES ('007', '0');
INSERT INTO public.members (members_user_id, borrow_num) VALUES ('009', '0');
INSERT INTO public.members (members_user_id, borrow_num) VALUES ('010', '0');
INSERT INTO public.members (members_user_id, borrow_num) VALUES ('011', '0');
INSERT INTO public.members (members_user_id, borrow_num) VALUES ('012', '0');


INSERT INTO public.librarians (librarians_user_id, username, password) VALUES ('001', 'Ross', '1234');
INSERT INTO public.librarians (librarians_user_id, username, password) VALUES ('002', 'Jack', '1234');
INSERT INTO public.librarians (librarians_user_id, username, password) VALUES ('005', 'Katz', '1234');
INSERT INTO public.librarians (librarians_user_id, username, password) VALUES ('008', 'Salton', '1234');


INSERT INTO public.documents (doc_id, title, user_id) VALUES ('2', 'Database Research', '');
INSERT INTO public.documents (doc_id, title, user_id) VALUES ('3', 'Clash and Clan', '');
INSERT INTO public.documents (doc_id, title, user_id) VALUES ('1', 'Operating System', '003');
INSERT INTO public.documents (doc_id, title, user_id) VALUES ('8', 'JANE EYRE', NULL);
INSERT INTO public.documents (doc_id, title, user_id) VALUES ('9', 'A Song of Ice and Fire', NULL);
INSERT INTO public.documents (doc_id, title, user_id) VALUES ('10', 'BASIC READERS', NULL);
INSERT INTO public.documents (doc_id, title, user_id) VALUES ('11', 'GONE WITH HTE  WIND', NULL);
INSERT INTO public.documents (doc_id, title, user_id) VALUES ('12', 'The Great Blue Yonder', NULL);
INSERT INTO public.documents (doc_id, title, user_id) VALUES ('4', 'communiction systems', '003');
INSERT INTO public.documents (doc_id, title, user_id) VALUES ('5', 'digital twin', '003');
INSERT INTO public.documents (doc_id, title, user_id) VALUES ('6', 'CMOS', '003');
INSERT INTO public.documents (doc_id, title, user_id) VALUES ('7', 'Electric Circuits', NULL);


INSERT INTO public.books (books_doc_id, edition, year) VALUES ('1', '5th', '2021-05-01');
INSERT INTO public.books (books_doc_id, edition, year) VALUES ('2', '1th', '2000-10-11');
INSERT INTO public.books (books_doc_id, edition, year) VALUES ('3', '1th', '2015-06-20');
INSERT INTO public.books (books_doc_id, edition, year) VALUES ('4', '2th', '2010-01-17');
INSERT INTO public.books (books_doc_id, edition, year) VALUES ('5', '6th', '2015-06-20');
INSERT INTO public.books (books_doc_id, edition, year) VALUES ('6', '5th', '2018-03-22');
INSERT INTO public.books (books_doc_id, edition, year) VALUES ('7', '3th', '2020-04-10');
INSERT INTO public.books (books_doc_id, edition, year) VALUES ('8', '2th', '2011-01-25');
INSERT INTO public.books (books_doc_id, edition, year) VALUES ('9', '7th', '2017-03-03');
INSERT INTO public.books (books_doc_id, edition, year) VALUES ('10', '8th', '2020-03-10');
INSERT INTO public.books (books_doc_id, edition, year) VALUES ('11', '9th', '2013-09-25');
INSERT INTO public.books (books_doc_id, edition, year) VALUES ('12', '12th', '2021-08-10');





INSERT INTO public.journalarticles (journal_articles_doc_id, journal_id) VALUES ('2', '2');
INSERT INTO public.journalarticles (journal_articles_doc_id, journal_id) VALUES ('1', '1');
INSERT INTO public.journalarticles (journal_articles_doc_id, journal_id) VALUES ('3', '3');
INSERT INTO public.journalarticles (journal_articles_doc_id, journal_id) VALUES ('4', '4');
INSERT INTO public.journalarticles (journal_articles_doc_id, journal_id) VALUES ('5', '5');
INSERT INTO public.journalarticles (journal_articles_doc_id, journal_id) VALUES ('6', '6');
INSERT INTO public.journalarticles (journal_articles_doc_id, journal_id) VALUES ('7', '7');
INSERT INTO public.journalarticles (journal_articles_doc_id, journal_id) VALUES ('8', '8');
INSERT INTO public.journalarticles (journal_articles_doc_id, journal_id) VALUES ('9', '9');
INSERT INTO public.journalarticles (journal_articles_doc_id, journal_id) VALUES ('10', '10');
INSERT INTO public.journalarticles (journal_articles_doc_id, journal_id) VALUES ('11', '11');
INSERT INTO public.journalarticles (journal_articles_doc_id, journal_id) VALUES ('12', '12');


INSERT INTO public.magazines (magazines_doc_id, name, publish_time, issue_id) VALUES ('3', 'Super Cell', '2021-12-05', '2');
INSERT INTO public.magazines (magazines_doc_id, name, publish_time, issue_id) VALUES ('1', 'memory', '2018-10-01', '1');
INSERT INTO public.magazines (magazines_doc_id, name, publish_time, issue_id) VALUES ('2', 'optimizer', '2021-01-20', '3');
INSERT INTO public.magazines (magazines_doc_id, name, publish_time, issue_id) VALUES ('4', 'query', '2001-02-09', '4');
INSERT INTO public.magazines (magazines_doc_id, name, publish_time, issue_id) VALUES ('5', 'projection', '2011-04-12', '5');
INSERT INTO public.magazines (magazines_doc_id, name, publish_time, issue_id) VALUES ('6', 'recovery', '2003-10-29', '6');
INSERT INTO public.magazines (magazines_doc_id, name, publish_time, issue_id) VALUES ('7', 'result', '2008-06-15', '7');
INSERT INTO public.magazines (magazines_doc_id, name, publish_time, issue_id) VALUES ('8', 'application', '2009-08-11', '10');
INSERT INTO public.magazines (magazines_doc_id, name, publish_time, issue_id) VALUES ('9', 'open', '2019-07-23', '12');
INSERT INTO public.magazines (magazines_doc_id, name, publish_time, issue_id) VALUES ('10', 'writer', '2021-07-19', '9');
INSERT INTO public.magazines (magazines_doc_id, name, publish_time, issue_id) VALUES ('11', 'data', '2001-01-01', '8');
INSERT INTO public.magazines (magazines_doc_id, name, publish_time, issue_id) VALUES ('12', 'encryption', '2014-03-09', '11');


INSERT INTO public.journal (journal_id, title, type, issue_id) VALUES ('2', 'Nature', 'Science', '2');
INSERT INTO public.journal (journal_id, title, type, issue_id) VALUES ('1', 'system', 'Science', '1');
INSERT INTO public.journal (journal_id, title, type, issue_id) VALUES ('3', 'data', 'Science', '3');
INSERT INTO public.journal (journal_id, title, type, issue_id) VALUES ('4', 'TTL', 'electric', '4');
INSERT INTO public.journal (journal_id, title, type, issue_id) VALUES ('5', 'story', 'novel', '5');
INSERT INTO public.journal (journal_id, title, type, issue_id) VALUES ('6', 'love', 'philosophy ', '6');
INSERT INTO public.journal (journal_id, title, type, issue_id) VALUES ('7', 'live', 'philosophy ', '7');
INSERT INTO public.journal (journal_id, title, type, issue_id) VALUES ('8', 'ptyhon', 'Science', '8');
INSERT INTO public.journal (journal_id, title, type, issue_id) VALUES ('9', 'C++', 'Science', '9');
INSERT INTO public.journal (journal_id, title, type, issue_id) VALUES ('10', 'JAVA', 'Science', '10');
INSERT INTO public.journal (journal_id, title, type, issue_id) VALUES ('11', 'query', 'Science', '11');
INSERT INTO public.journal (journal_id, title, type, issue_id) VALUES ('12', 'memory', 'Science', '12');


INSERT INTO public.author (author_name, address) VALUES ('peter jackson', 'long island');
INSERT INTO public.author (author_name, address) VALUES ('J.Robinson', 'UK');
INSERT INTO public.author (author_name, address) VALUES ('H.LU', 'US');
INSERT INTO public.author (author_name, address) VALUES ('K.Mikkilineni', 'Japan');
INSERT INTO public.author (author_name, address) VALUES ('Hennessy', 'Korea');
INSERT INTO public.author (author_name, address) VALUES ('R.Hevenr', 'Fance');
INSERT INTO public.author (author_name, address) VALUES ('Bo Yao', 'Italy');
INSERT INTO public.author (author_name, address) VALUES ('Smith', 'Thailand');
INSERT INTO public.author (author_name, address) VALUES ('Gary', 'New Zealand');
INSERT INTO public.author (author_name, address) VALUES ('Johnson', 'Brazil');
INSERT INTO public.author (author_name, address) VALUES ('Carey', 'Germany');
INSERT INTO public.author (author_name, address) VALUES ('Layman', 'China');


INSERT INTO public.publisher (publisher_name, address, phone) VALUES ('Abc', 'UK', '123456');
INSERT INTO public.publisher (publisher_name, address, phone) VALUES ('Bcd', 'US', '234567');
INSERT INTO public.publisher (publisher_name, address, phone) VALUES ('Cde', 'China', '345678');
INSERT INTO public.publisher (publisher_name, address, phone) VALUES ('Def', 'Japan', '456789');
INSERT INTO public.publisher (publisher_name, address, phone) VALUES ('Efg', 'Korea', '567890');
INSERT INTO public.publisher (publisher_name, address, phone) VALUES ('Fgh', 'Fance', '678901');
INSERT INTO public.publisher (publisher_name, address, phone) VALUES ('Ghi', 'Norway', '789012');
INSERT INTO public.publisher (publisher_name, address, phone) VALUES ('Hij', 'Germany', '890123');
INSERT INTO public.publisher (publisher_name, address, phone) VALUES ('Ijk', 'Italy', '901234');
INSERT INTO public.publisher (publisher_name, address, phone) VALUES ('Jku', 'Brazil', '012345');
INSERT INTO public.publisher (publisher_name, address, phone) VALUES ('Kuv', 'New Zealand', '001234');
INSERT INTO public.publisher (publisher_name, address, phone) VALUES ('Uvw', 'Thailand', '120034');


INSERT INTO public.issue (issue_id) VALUES ('1');
INSERT INTO public.issue (issue_id) VALUES ('2');
INSERT INTO public.issue (issue_id) VALUES ('3');
INSERT INTO public.issue (issue_id) VALUES ('4');
INSERT INTO public.issue (issue_id) VALUES ('5');
INSERT INTO public.issue (issue_id) VALUES ('6');
INSERT INTO public.issue (issue_id) VALUES ('7');
INSERT INTO public.issue (issue_id) VALUES ('8');
INSERT INTO public.issue (issue_id) VALUES ('9');
INSERT INTO public.issue (issue_id) VALUES ('10');
INSERT INTO public.issue (issue_id) VALUES ('11');
INSERT INTO public.issue (issue_id) VALUES ('12');


INSERT INTO public.journalissue (issue_id, date) VALUES ('1', '2021-12-05');
INSERT INTO public.journalissue (issue_id, date) VALUES ('2', '2021-12-04');
INSERT INTO public.journalissue (issue_id, date) VALUES ('3', '2021-12-02');
INSERT INTO public.journalissue (issue_id, date) VALUES ('4', '2021-11-26');
INSERT INTO public.journalissue (issue_id, date) VALUES ('5', '2021-11-20');
INSERT INTO public.journalissue (issue_id, date) VALUES ('6', '2021-11-15');
INSERT INTO public.journalissue (issue_id, date) VALUES ('7', '2021-11-05');
INSERT INTO public.journalissue (issue_id, date) VALUES ('8', '2021-11-01');
INSERT INTO public.journalissue (issue_id, date) VALUES ('9', '2021-10-25');
INSERT INTO public.journalissue (issue_id, date) VALUES ('10', '2021-10-18');
INSERT INTO public.journalissue (issue_id, date) VALUES ('11', '2021-10-05');
INSERT INTO public.journalissue (issue_id, date) VALUES ('12', '2021-09-22');


INSERT INTO public.magazineissue (issue_id, title) VALUES ('2', 'tecent');
INSERT INTO public.magazineissue (issue_id, title) VALUES ('1', 'oxford');
INSERT INTO public.magazineissue (issue_id, title) VALUES ('3', 'tree');
INSERT INTO public.magazineissue (issue_id, title) VALUES ('4', 'kite');
INSERT INTO public.magazineissue (issue_id, title) VALUES ('5', 'robinson');
INSERT INTO public.magazineissue (issue_id, title) VALUES ('6', 'crusoe');
INSERT INTO public.magazineissue (issue_id, title) VALUES ('7', 'sophie');
INSERT INTO public.magazineissue (issue_id, title) VALUES ('8', 'ferry');
INSERT INTO public.magazineissue (issue_id, title) VALUES ('9', 'rooney');
INSERT INTO public.magazineissue (issue_id, title) VALUES ('10', 'sally');
INSERT INTO public.magazineissue (issue_id, title) VALUES ('11', 'stilton');
INSERT INTO public.magazineissue (issue_id, title) VALUES ('12', 'geronimo');


INSERT INTO public.copies (copy_id, location, borrowed, doc_id, members_user_id, due_date) VALUES ('2', 'first floor', '0', '1', '', NULL);
INSERT INTO public.copies (copy_id, location, borrowed, doc_id, members_user_id, due_date) VALUES ('3', 'first floor', '0', '1', '', NULL);
INSERT INTO public.copies (copy_id, location, borrowed, doc_id, members_user_id, due_date) VALUES ('4', 'first floor', '0', '1', '', NULL);
INSERT INTO public.copies (copy_id, location, borrowed, doc_id, members_user_id, due_date) VALUES ('5', 'second floor', '1', '2', '1', '2022-01-06');
INSERT INTO public.copies (copy_id, location, borrowed, doc_id, members_user_id, due_date) VALUES ('6', 'second floor', '1', '2', '1', '2022-01-06');
INSERT INTO public.copies (copy_id, location, borrowed, doc_id, members_user_id, due_date) VALUES ('1', 'first floor', '1', '1', '1', '2022-01-06');
INSERT INTO public.copies (copy_id, location, borrowed, doc_id, members_user_id, due_date) VALUES ('7', 'second floor', '1', '3', '1', '2021-01-06');


INSERT INTO public.libdoc (doc_id, librarians_user_id) VALUES ('1', '001');
INSERT INTO public.libdoc (doc_id, librarians_user_id) VALUES ('2', '002');
INSERT INTO public.libdoc (doc_id, librarians_user_id) VALUES ('5', '005');


INSERT INTO public.cite (doc_id, cite_id) VALUES ('1', '1');
INSERT INTO public.cite (doc_id, cite_id) VALUES ('2', '2');
INSERT INTO public.cite (doc_id, cite_id) VALUES ('3', '3');


INSERT INTO public.writtenby (books_doc_id, journal_articles_doc_id, author_name) VALUES ('1', '1', 'peter jackson');
INSERT INTO public.writtenby (books_doc_id, journal_articles_doc_id, author_name) VALUES ('2', '2', 'J.Robinson');
INSERT INTO public.writtenby (books_doc_id, journal_articles_doc_id, author_name) VALUES ('3', '3', 'H.LU');
INSERT INTO public.writtenby (books_doc_id, journal_articles_doc_id, author_name) VALUES ('4', '4', 'K.Mikkilineni');
INSERT INTO public.writtenby (books_doc_id, journal_articles_doc_id, author_name) VALUES ('5', '5', 'Hennessy');
INSERT INTO public.writtenby (books_doc_id, journal_articles_doc_id, author_name) VALUES ('6', '6', 'R.Hevenr');
INSERT INTO public.writtenby (books_doc_id, journal_articles_doc_id, author_name) VALUES ('7', '7', 'Bo Yao');
INSERT INTO public.writtenby (books_doc_id, journal_articles_doc_id, author_name) VALUES ('8', '8', 'Smith');
INSERT INTO public.writtenby (books_doc_id, journal_articles_doc_id, author_name) VALUES ('9', '9', 'Gary');
INSERT INTO public.writtenby (books_doc_id, journal_articles_doc_id, author_name) VALUES ('10', '10', 'Johnson');
INSERT INTO public.writtenby (books_doc_id, journal_articles_doc_id, author_name) VALUES ('11', '11', 'Carey');
INSERT INTO public.writtenby (books_doc_id, journal_articles_doc_id, author_name) VALUES ('12', '12', 'Layman');


INSERT INTO public.publishedby (publisher_name, journal_id, books_doc_id) VALUES ('Abc', '1', '1');
INSERT INTO public.publishedby (publisher_name, journal_id, books_doc_id) VALUES ('Bcd', '2', '2');
INSERT INTO public.publishedby (publisher_name, journal_id, books_doc_id) VALUES ('Cde', '3', '3');
INSERT INTO public.publishedby (publisher_name, journal_id, books_doc_id) VALUES ('Def', '4', '4');
INSERT INTO public.publishedby (publisher_name, journal_id, books_doc_id) VALUES ('Efg', '5', '5');
INSERT INTO public.publishedby (publisher_name, journal_id, books_doc_id) VALUES ('Fgh', '6', '6');
INSERT INTO public.publishedby (publisher_name, journal_id, books_doc_id) VALUES ('Ghi', '7', '7');
INSERT INTO public.publishedby (publisher_name, journal_id, books_doc_id) VALUES ('Hij', '8', '8');
INSERT INTO public.publishedby (publisher_name, journal_id, books_doc_id) VALUES ('Ijk', '9', '9');
INSERT INTO public.publishedby (publisher_name, journal_id, books_doc_id) VALUES ('Jku', '10', '10');
INSERT INTO public.publishedby (publisher_name, journal_id, books_doc_id) VALUES ('Kuv', '11', '11');
INSERT INTO public.publishedby (publisher_name, journal_id, books_doc_id) VALUES ('Uvw', '12', '12');


INSERT INTO public.documentdescription (types, keywords, classification, doc_id) VALUES ('computer', 'linux', 'science', '1');
INSERT INTO public.documentdescription (types, keywords, classification, doc_id) VALUES ('computer', 'database', 'science', '2');
INSERT INTO public.documentdescription (types, keywords, classification, doc_id) VALUES ('game', 'game, phone', 'play', '3');
INSERT INTO public.documentdescription (types, keywords, classification, doc_id) VALUES ('Electric', 'communiction', 'science', '4');
INSERT INTO public.documentdescription (types, keywords, classification, doc_id) VALUES ('computer', 'digital', 'science', '5');
INSERT INTO public.documentdescription (types, keywords, classification, doc_id) VALUES ('Electric', '5G', 'science', '6');
INSERT INTO public.documentdescription (types, keywords, classification, doc_id) VALUES ('Electric', '4G', 'science', '7');
INSERT INTO public.documentdescription (types, keywords, classification, doc_id) VALUES ('novel', 'jane', 'literature', '8');
INSERT INTO public.documentdescription (types, keywords, classification, doc_id) VALUES ('novel', 'song', 'literature', '9');
INSERT INTO public.documentdescription (types, keywords, classification, doc_id) VALUES ('novel', 'basic', 'literature', '10');
INSERT INTO public.documentdescription (types, keywords, classification, doc_id) VALUES ('novel', 'wind', 'literature', '11');
INSERT INTO public.documentdescription (types, keywords, classification, doc_id) VALUES ('novel', 'blue', 'literature', '12');


INSERT INTO public.editor (editor, issue_id) VALUES ('thomas edison', '1');
INSERT INTO public.editor (editor, issue_id) VALUES ('tommy young', '2');
